Sept 9
=========

Importance of Microbes in Nature
-------------------------
 
- Widespread distribution
	- Easily dispersed
	- Physiologically diverse
	- Present for > 3 billion years

- Abundance
	- Small size, but large numbers
		- Average of 2 microns
	- Estimated that one hectare of fertile soil, 15 cm deep has 1000 kg of microbes
<br></br>
- Adaptability
	- Short generation time
	- Quick adaptation due to molecular regulation
<br></br>
- Survival
	- Long survival on minimal energy
	- Many live in extreme environments
<br></br>
- Microbial population
	- Clonal daughter cells are derived from parents by cell division
<br></br>
- Habitat
	- Physical location where a microbe lives
<br></br>
- Microbial community
	- Multiple, interacting populations
	- Diverse and abundant
	- Regulated by resources and environmental constraints
<br></br>
- Diversity of metabolism
	- Anaerobic respiration
	- Chemolithotrophy
	- Nitrogen fixation
	- Biodegradation  of xenobiotics
	- Aerobic respiration
	- Photosynthesis
<br></br>
- Mixed populations work together
	- Different metabolic functions coordinate to perform novel activities
	- Microbes function as communities, not individual cells
 
Relationship of Microbes to the Environment
-------------------------

- Microenvironment
	- The immediate surroundings of microbial cells
<br></br>
- **Habitat**
	- Physical location where microbes exist
<br></br>
- **Niche**
	- Functional role fulfilled by an organism or population
<br></br>
- **Gradients**
	- Physical parameters which change over distance or time
<br></br>
- **Microbial ecology**
	- Study of microbial relationships in their natural environments
	- Environment comprises two components
		- Biotic
			- Other organisms, their metabolic byproducts, food, etc
		- Abiotic
			- Air, water, land, etc
<br></br>
- Difference between microbial physiologist and a microbial ecologist?
	- Physiologist = Capabilities
	- Ecologist = Activities, livelihood

Tasks of a Microbial Ecologist
-------------------------

- Identification characterization of species

- Determination of specific functions or activities

- Characterize microenvironment

Sept 11
===================

Origin of Life
-------------------------

- First bacteria emerged ~ 3.8 bya
	- Were anaerobic fermenters
<br></br>
- Anoxygenic phototrophic bacteria emerged ~ 3.5 bya
<br></br>
- Cyanobacteria ~ 2.6 bya
	- Origin of molecular oxygen on Earth
	- Water is an inefficient electron donor for photosynthesis, but it is very abundant
<br></br>
- Eukaryotes were 2 bya

- Multicellular life emerges < 1 bya
 
Why study microbial ecology?
-------------------------

- Microbes live in all ecosystems
- Microbes carry out unique and important activities not performed by other organisms

History of Microbial Ecology
-------------------------

- Major historical developents follow philosophical and technological advances
	- Driven by research questions
<br></br>
- Antonie van Leeuwenhoek developed first simple microscope in 1680s
	- Could see protists and large bacteria
	- Investigated microbes in all environments
<br></br>
- Desire to prevent spoilage of wine and beer in the 1860s drove research
	- Louis Pasteur and John Tyndall
	- Eradiacation is possible after discrediting of spontaneous generation
	- Sterilization and pasteurization developed
	- Discovery that spoilage is due to biological and not simply chemical processes
	- Dissemination of bacteria through air discovered
<br></br>
- Joseph Lister, Robert Koch, Fannie Hesse (1880s)
	- Developed media and processes to grow pure cultures
		- First potato slices, then agar
	- Found specific diseases were caused by specific bacteria
	- Monomorphism of bacteria
<br></br>
- Winogradski and Beijerinck were founders of microbial ecology
	- Studies environmental	conditions and corresponding microbes
	- Developed concept of environmental selectivity
		- "Everything is everywhere; The environment selects"
	- Devised enrichment culture technique
<br></br>
- **Winogradski column**
	- Artificial microbial ecosystem
	- Partly filled with sulfide-rich mud, with carbon substrates, covered with water
		- CaCO<sub>3</sub> as buffer and CaSO<sub>4</sub> as sulfate source
	- Grows phototrophic, anaerobic, and microaerophilic soil bacteria
	- Allowed discovery of **sulfate reducing bacteria**
	- Demonstrated oxidation of hydrogen sulfide, sulfur, and ferrous iron
	- Conclusively demonstrated chemoautotrophy (using *Beggiatoa* and nitrifiers)
	- Nitrogen transformation

Sept 13
===================

History of Microbial Ecology Con't
-------------------------

- Winogradsky Column Con't
	- Gradients form
		- O<sub>2</sub> higher at top, decreasing with depth
		- Sulfate is high at bottom, decreasing with height
<br></br>
- How does a microbe grow in its environment?
	- Kluyver, van Niel, Schlegel, & the Delft School of Microbiology
		- Followers of Beijerinck
		- Developed comparative and ecological microbial physiology and biochemistry
		- Related microbes to environmental conditions
		- isolated phototrophs and chemolithoautotrophs, reported their physiological diversity
		- applied thermodynamics and energy calculations to microbial physiology
		- compared different organisms, revealed unifying metabolism in microbial world
		- Developed "Unity of biochemistry concept"
 
Physiology and Habitats
-------------------------

- Physical and chemical effects on physiology
	- Temperature
	- Radiation
	- Water activity / salinity
	- pH
	- Eh (redox)
	- Oxygen
<br></br>
- Habitats
	- Terrestrial
		- Soil, subsurface, deserts
	- Aquatic
		- Lakes
		- Oceans
		- Ice
- Growth, metabolism, an survival
	- Nested categories
<br></br>
- Extremes limit diversity
<br></br>
- Extremes are often sources of novel enzymes
	- Industrial applications
		- **Taq** polymerase
<br></br>
- Optimal growth temperature can be used to classify microbes
	- **Psychrophiles** (< 20 C)
	- **Mesophiles** (25-45 C)
	- **Thermophiles** (50-75 C)
	- **Hyperthermophiles** (> 80 C)
<br></br>
- Higher temperature usually means higher activities
<br></br>
- **Hyperthermophiles** tend to be anaerobic and/or chemolithoautotrophs
	- Eukarya excluded above 60 C
	- Cyanobacteria limited at 73 C
	- Most known are archaea
		- Most are crenarchaeota
	- Habitats include
		- Hotsprings
		- hydrothermal vents
	- Limit is likely 150 as ATP degrades at this temperature
<br></br>
- **"Strain 121"** highest known growth temperature
	- 121 C
	- iron reducer
		- Excretes magnetite
	- Survives autoclaving 

Sept 16
=================

Adaptations
-------------------------

- No 'universal feature' that imparts (hyper)thermophily
	- Some hyperthermophilic Archaea have 'thermoprotectants'
		- Small molecule or salt solutes
	- Protein modifications
		- Increase in ionic and hydrophobic bonds
		- Lots of chaperonins
	- DNA
		- Some hyperthermophilic Archaea have gyrase that supercoils in opposite direction
		- Extra DNA binding histones
		- Extra magnesium for thermostability
	- Lipids
		- Increase in chain length
		- Increase in saturation
		- Conversion to lipid monolayer
		- Some Archaea have ether linked linear chains rather than isoprenoids
 
- Endospores are extremely heat, drying, and freezing
	- Insurance for cell
<br></br>
- Psychrophiles grow at 0 C, optimal at <15 C, die above 20 C
	- In general limits are based on liquid water
	- Growth observed to -17 C
	- Metabolism to -25 C
	- e.g. *Polaromonas vacuolata* - pink colour on snow
		- Optimal growth around 4 C
		- No growth above 12 C
	- Adaptations
		- Proteins
			- More alpha-helix and less beta-sheet domains
				- More flexible, but more easily denatured
		- Lipids
			- Increased branching, unsaturation, short-chain
	- Most microbes are cold-tolerant

Radiation
-------------------------

- Visible, UV, short-IR, and gamma have largest impact on life
	- Infrared used by anoxygenic photosynthetic bacteria
	- Visible spectrum used for nine types of phototrophs

Sept 18
========================

Photosynthesis
-------------------------
 
- Chlorophylls found in cyanobacteria and plants
	- Mg 2+ ion in centre of molecule
		- Rarely, Zn is used
<br></br>
- **Bacteriochlorophylls** found in other bacteria
	- Bacteriopheophytin is precursor molecule
	- Very structurally similar to chlorophyll
		- CH<sub>3</sub>-CO- substituted for H<sub>2</sub>C = CH
<br></br>
- **Carotenoids** can function as photosynthetic pigments
	- Also serve as protectants by absorbing damaging radiation
	- Scavenge Reactive Oxygen Species (ROS) and heavy metal ions
<br></br>
- **Photophosphoylation**
	- Purple bacteria
	- Photosynthesis variant
	- Uses molecule "P870", BChl, BPh, NADH, Cyt bc1, Cyt c2
		- P870 refers to absorbing 870 nm light
	- Anoxygenic
	- Electrons are transferred cyclically
		- Exciton replaces lost energy
		- Powers proton pump to push protons from cytoplasm to periplasm
		- Proton gradient is used to power ATP synthase
 <br></br>
- Non-cyclic photosynthesis
	- Used by cyanobacteria and plants
	- Non-cyclic component (Photosystem II) powers a cyclic component (Photosystem I)
		- "Z-scheme"
	- Uses P680, PH, Cyt bf, P700, Chl, Fd, Fp, NADPH
	- Electron donor is H<sub>2</sub>0
		- Photolysis of H<sub>2</sub>0
			- 2 H<sub>2</sub>0 --> O<sub>2</sub> + 4H

Photosynthetic Groups
-------------------------
- **Purple Sulfur Bacteria**
	- *Chromatium* spp.
	- Gamma-Proteobacteria
	- Anoxygenic phototroph
		- Obligate anaerobes
	- Obligately use reduced sulfur as electron acceptor
	- Bacteriochlorophyll *a* and carotenoids
	- Found in sulfur springs, and salty marshes and lakes
<br></br> 
- **Purple Non-Sulfur Bacteria**
	- *Rhodopseudomonas* spp.
	- Alpha-, beta-Proteobacteria
	- Anoxygenic phototroph
		- Facultative anaerobes
	- May use low [reduced sulfur] as electron acceptor, but prefer organics for this purpose
	- Bacteriochlorophyll *a* and carotenoids
	- Found in sulfur springs and waste lagoons
<br></br>
- **Green Sulfur Bacteria**
	- *Chlorobium* spp.
	- Anoxygenic
	- Obligate anaerobes
	- Bacteriochlorophyll *c*, *d*, or *e*
		- Small amounts of Bacteriochlorophyll *a*
	- Carotenoids
	- Aquatic habitats, bottom of lakes
	- Have chlorosomes
		- Structures to harvest light
		- Highly specialized antenna complex
		- Have antenna pigments, carotenoids, and bacteriochlorophyll *c*
<br></br> 
- **Green Non-Sulfur Bacteria**
	- *Chloroflexus* spp.
	- Anoxygenic
	- Facultative anaerobes
	- Filamentous
	- Bacteriochlorophyll *c*, *a*
	- Carotenoids
	- Found in hot, alkaline springs
	- Have chlorosomes
		- Structures to harvest light
		- Highly specialized antenna complex
		- Have antenna pigments, carotenoids, and bacteriochlorophyll *c*
<br></br>
- **Heliobacteria**
	- Firmicutes
		- Only Gram-positive photosyntetic family
	- Anoxygenic, obligate anaerobes
	- Heterotrophic
	- Bacteriochlorophyll *g*
	- Found in soils
	- Many species can fix nitrogen
	- *Heliobacterium* spp.

Sept 20
===================

Photosynthetic Groups Con't
-------------------------

- **Obligate aerobic anoxygenic phototrophic bacteria**
	- Bacteriochlorophyll *a*, lots of carotenoids
	- Photoheterotrophic
	- Marine (even deep ocean)
	- Diverse morphology
	- 5-16% of total prokaryotes in Atlantic Ocean
	- *Erythrobacter* spp.
<br></br>
- **Cyanobacteria**
	- Oxygenic
	- Chlorophyll *a*
	- Morphologically diverse
		- Many form filamentous trichomes
	- Thylakoid membranes with phycobilisomes contain phycobilin accessory pigments
	- *Spirulina*, *Nostoc*, and *Anabena* spp.
<br></br>
- **Prochlorophytes**
	- Oxygenic
	- Chlorophyll *a* and *b*
	- Often very small cells (0.5, 0.8 micrometres)
	- Numerically dominates photosyntetic community in tropical and subtropical oceans

Radiation
-------------------------

- Light intensity decreases with depth because of absorbtion and scattering

- Light intensity affects rate of photosynthesis

- Quality of light also changes with depth
	- Short wavelengths penetrate deeper

- Phototrophs arrange themselves along the light gradient
	- Always the same ordering

Sept 23
=========================

Ionizing Radiation
-------------------------

- Ultraviolet light is highly energetic

- Purine and pyrimidine bases absorb UV at 260 nm

- Pyrimidine dimers formed
	- Co-valent bonds
	- Replication is prevented

- Can be used to sterilize equipment, kinda sorta

- Microbes adapt to prevent damage from UV in environment
	- Phototaxis in cyanobacteria
	- Carotenoids give photoprotection
	- DNA repair replace T-T dimers
	- Cyanobacteria produce **scytonemin**
		- Aromatic rings absorb UV
		- Deposited in extra cellular sheath
		- Can absorb 85-90% of UV
<br></br>
- X-ray, gamma, and cosmic rays
	- Ionize water, forming free radiacals
		- React with DNA
	- Low levels can trigger mutations
	- High levels destroy nucleic acids and proteins
		- Double-stranded breaks are most dangerous
			- Repairable by homologous recombination
			- More than a few breaks cannot be fixed
	- Endospores have notably high resistance to gamma radiation

	- Most resistant organism is *Deinococcus radiodurans*
		- Can survive up to 1.5 million rads (<1000 rads will kill a human)
		- Can survive many double-stranded breaks
		- Has tightly-coiled rings of DNA in three of the four tetrad cells
		- Rings hold broken DNA together so it can be repaired
		- Exchanges DNA between cells in tetrad to further repair damage
		- Open rings are used for making protein

Water Activity
-------------------------

- Liquid water is essential for microbial life
	- Necessary for cell structure and functions
<br></br>
- Availability: **water activity** aw
	- Depends on moles of water
	- Moles of solute and activity coefficients
	- Adsorption to solid surfaces
	- Measured as relative humidity at equilibrium
	- Scale of 0 to 1
		- Totally dry to pure water

- Most microbes need aw >0.95

- **Xerophiles**
	- Grow with low water activity
	- Lichens
	- Most fungi as low as 0.70
	- Halophilic bacteria and Archaea
		- *Ectothiorhodospira* = 0.85
		- *Halobacterium* = 0.75
<br></br>
- Environments
	- Meat = 0.99
	- Bread = 0.95
	- Cheese = 0.85
	- Jams = 0.80
	- Cereals = 0.75
	- Dried fruit = 0.60
	- Biscuits = 0.30
	- Instant coffee = 0.20
<br></br>
- Osmotic Pressure
	- Difference in solute concentration across membrane
	- High solute outside, water leaves cell
		- And *vice versa*
	- **Osmotolerant** organisms tolerate high [organic solutes]
	- **Osmophiles** require high [organic solutes]
	- Adaptations to osmotic pressure
		- Rigid cell wall to resist turgor pressure (1000 kPa)
		- Accumulation of compatible solutes
			- Glycine betaine, sucrose, trehalose, gylcerol, DMSP
		- If environment becomes diluted, can excrete, degrade, or polymerize compatible solutes
<br></br>
- Salinity
	- Generally the major salt is NaCl
	- Oceans, brines, salt ponds, sea ice
	- Sodium affects membrane potential
	- **Halotolerant** organisms (0-15% NaCl)
		- *Staphylococcus aureus*
	- **Halophiles** (2-20% NaCl)
		- *Vibrio fisheri* and most marine microbes
	- **Extreme halophiles** (12-32% NaCl (saturated))
		- *Ectothiorhodospira* (purple sulfur bacteria)
		- *Dunaliella* (green algae)
		- *Halobacterium* (red Archaea)
	- Microbes can be trapped in salt inclusions for 10^5 years to 10^8 years

Sept 25
=========================

Salinity Con't
------------------------

- Adaptations to extreme salinity
	- Bacteria
		- Accumulation of organic compatible solutes
			- Synthesized or transported from environment
				- Gycline betaine, ectoine, trehalose, DMSP, proline
		- Temporary accumulation of small amounts of K+
	- Archaea
		- Accumulation of K+ at high concentration (5 M)
			- Enzymes function best in presence of K+

pH
-------------------------

- pH affects
	- cell structures
		- hydrolysis of polymers and protein denaturation
	- dissociation of solutes
<br></br>
- Acidophiles
	- Found in mine tailings, acidic springs, stomach, some foods
	- Many actually lower the pH of their environment via metabolic activity
	- *Lactobacillus* spp. produce lactic acid (food, teeth)
	- *Thiobacillus thiooxidans*, *T. ferrooxidans*, and *Sulfolobus* spp. produce sulfuric acid (mines)
	- Most acid tolerant organism is *Picrophilus* spp.
		- Found in different solfataric locations in Japan
		- pH optimum is 0.7, grow as low as -0.6
<br></br>
- Alkaliphiles
	- Found in saline lakes and springs, soda lakes
		- *Bacillus*, *Chloroflexus*, *Spirulina*, and *Halobacterium* spp.
	- Various Proteobacteria and Gram-postive bacteria found growing at pH 12.8
	- Source of industrially important proteases and lipases for detergents
<br></br>
- Adaptations to extreme pH
	- pH range of 2-3 units for most microbes
	- Keep cytoplasm closer to neutrality than external environment
		- pH 4.6 - 9.5
		- *T. ferrooxidans* has external environment of pH 2.0, but internal 6.0 to 6.5
	- Proton pumps, basic amino acids, DNA/protein repair systems
	- Low membrane H+ permeability

Eh (redox potential)
-------------------------

- Many metabolic reactions are redox reactions
	- Can be measured with standard hydrogen electrode
		- Donates e- has -Eh
		- Accepts e- has +Eh
	- Low Eh favours anaerobic reactions
	- High Eh favours aerobic reactions
<br></br>
- Can change rapidly (over minutes) and over short distances (over millimetres)
	-+800mV to -450 mV
<br></br>
- Affects chemicals that are important to cells
	- e.g. Fe availability

Oxygen
-------------------------

- Oxygenic > aerobic > microaerophilic > aerotolerant > facultative anaerobic > facultative aerobic > anaerobic > strict anaerobic

Sept 27
=========================

Oxygen Con't
-------------------------

- Problems facing aerobes
	- Oxygen exists in many toxic forms
		- O<sub>2</sub> is least toxic; good e- acceptor for respiration
		- O* (singlet oxygen) intermediate oxidizer
		- O<sub>3</sub> (ozone) strong oxidizer
		- O<sub>2</sub>- (superoxide anion) long-lived oxidizer
		- O<sub>2</sub>-2 (peroxide) quite toxic
		- OH (hydroxyl radical) most reactive, short-lived
<br></br>
- Adaptations to oxidizers
	- Enzymes to convert more toxic forms to less toxic forms
		- **Superoxide dismutase**
			- 2(O<sub>2</sub>-) + (H+) --> H<sub>2</sub>O<sub>2</sub> + O<sub>2</sub>
		- **Peroxidase**
			- H<sub>2</sub>O<sub>2</sub> + NADH+ --> 2(H<sub>2</sub>O) + NAD
		- **Catalase**
			- 2(H<sub>2</sub>O<sub>2</sub>) --> 2(H<sub>2</sub>O) + O<sub>2</sub>
<br></br>
- Cultivation of anaerobes
	- Must exclude oxygen
		- Boil water to remove dissolved O<sub>2</sub>
		- Substitute air with mixtures of N2 or H<sub>2</sub> with CO<sub>2</sub>
	- Lower Eh of medium
	- Add Na2S or other reductant to lower Eh
	- Use a glove bag with a catalyst (palladium) to remove O<sub>2</sub> traces
	- Dyes like resazurin monitor anaerobic conditions
	- Tolerance varies among anaerobes
		- Some anaerobes have catalase, superoxide dismutase, and peroxidase

Chemical nutrients
-------------------------

- Inorganics
	- Nutrients for biosynthesis (CO<sub>2</sub>, nitrate, phosphate, sulfate, iron, etc)
	- For energy (electron donors, acceptors)
	- Nutrients can be inhibitory at high concentration
<br></br>
- Organics
	- Critical for heterotrophs (most organisms) for biosynthesis and energy
	- Usable organic compounds often limited in natural environments
	- Chemical structure and physical properties affect use of organics

- Availability of chemicals
	- Diffusion
		- Different between aqueous and terrestrial environments
		- Depends on porosity of soil, and whether filled with water or gas
	- Adsorption
		- Restricts movement of chemicals
		- May accumulate limiting nutrients on a surface
	- Solid organics
		- Most organic substrates are polymers
			- e.g. Plants: 50% cellulose, 10-25% lignin, 10-20% hemicelluloses and pectin, 10% protein, 2-5% lipids, <2% nucleic acids, <5% dissolved compounds
<br></br>
- Oligotrophy
	- Most organisms isolated are mesotrophs or eutrophs, yet most microbes in nature are oligotrophs (starvation is normal)
	- Adaptations
		- Low growth rate
		- Highly efficient substrate scavenging
		- Nutrient accumulation
			- Glycogen
			- Polysaccharides
		- Morphological adaptations
			- Reduction in size
				- Some Gram-negative bacteria can reduce size by 30x
				- Raises surface area to volume ration
			- Some change shape from rod to coccus
		- Life cycles
			- *Caulobacter* spp. attach to surfaces with appendages
			- *Bacillus* spp. form endospores
			- Myxobacteria like *Myxococcus xanthus* have complex life cycle triggered by starvation, forming myxospores

Sept 30
=========================

Biogeochemical Cycling of Elements
-------------------------

- Five Major Cycles
	- Carbon
	- Nitrogen
	- Sulfur
	- Phosphorus
	- Iron
<br></br>
- 1 Petagram = 1 gigatonne

- Carbon cycle
	- Terrestrial lithosphere contains 99% (mostly inorganic)
	- Exchanges with atmosphere are greatest and fastest
	- Sedimentation and burial sequester organic carbon
	- Fluxes between reservoirs tend to balance
	<br></br>
- Three kinds of carbon
	- Inorganic
		- Many aqueous environments buffered at near neutral pH by C02/HCO<sub>3</sub>-
		- Concentrations of carbon tend to be quite stable, in steady state
		- Abundant everywhere
<br></br>
	- Organic
		- Billions of different compounds
		- Includes carbon that makes up living organisms, but  more is present in dead and decaying material
		- Lots of organic carbon in subsurface, it is not actively cycled
<br></br>
	- Methane
		- Most reduced form of carbon
		- Very stable
		- Organic in structure, but inorganic in behaviour
		- Gas in atmosphere, or as solid hydrates on ocean floor
		- Natural gas or swamp gas
		- Potent greenhouse gas
		- Mars has 10 nmol / mol methane in atmosphere
			- May be indicative of biological activity
				- On Earth, nearly all methane is produced biologically

Oct 2
=========================

Microbes of the Carbon Cycle
-------------------------

- Carbon Fixation
	- CO<sub>2</sub> --> organic material (CH<sub>2</sub>O)
	- Autotrophs (get all carbon from inorganic C)
	- Measured as primary productivity
	- Often measured with radiolabelled carbon
	- Requires a chemical sources of electrons
	- Requires energy (ATP) from chemicals or light
		- Driving force behind all biogeochemical cycles
	- Two kinds of autotrophs
		- Chemolithoautotrophs
		- Photoautotrophs
<br></br>
- Chemolithoautotrophs
	- Chemo = chemical energy source
	- litho = "rock" (inorganic electron source)
	- auto = self
	- troph = feeding

	- Many use Calvin cycle
		- RuBisCo
			- Ribulose-1,5-Bisphosphate Carboxylase Oxygenase
			- Converts CO<sub>2</sub> + e- --> hexose and glyceraldehyde
	- Some use acetyl-CoA pathway (also called the carbon monoxide dehydrogenase or Wood-Ljungdahl pathway)
		- Key enzyme is CODH
			- Converts 2(CO<sub>2</sub>) + e- + CoA --> acetyl-CoA
			- Electron source is H<sub>2</sub>
	- Use reduced inorganic chemicals like hydrogen, ammonium, iron, or sulfide as sources of energy
	- Examples
		- Nitrifying bacteria
			- *Nitrosomonas* and * Nitrobacter* spp.
		- Sulfide oxidizers
			- *Thiobacillus* and *Beggiatoa* spp.
		- Iron oxidizers
			- *Gallionella* spp.
		- Hydrogen bacteria
			- *Hydrogenomonas* spp.
		- Acetogens (acetyl-CoA pathway)
			- *Acetobacterium* spp.
		- Sulfate reducers (acetyl-CoA pathway)
			- *Desulfobacterium* spp.
		- Methanogens (acetyl-CoA pathway)
			- *Methanobacterium* spp.
<br></br>
- Photoautotrophs
	- Oxygenic
		- cyanobacteria
		- prochlorophytes
			- *Prochlorothrix* spp.
		- algae (chloroplasts)
		- CO<sub>2</sub> + H<sub>2</sub>O --> CH<sub>2</sub>O + O<sub>2</sub>
		- Usually uses Calvin Cycle
			- e- from water
		- Need Photosystem II and Rubisco
		<br></br>
	- Anoxygenic photoautotrophs
		- Purple sulfur bacteria
			- *Chromatium* and *Ectothiorhodospira* spp.
		- Purple non-sulfur bacteria
			- *Rhodopseudomonas* spp.
		- Green sulfur bacteria
			- *Chlorobium* spp.
		- Green non-sulfur bacteria
			- *Chloroflexus* spp.

Oct 4
=========================

Autotrophy Con't
-------------------------

- Wide variety of pathways
	- Calvin Cycle
		- *Chromatium*, *Ectothiorhodospira*, *Rhodopseudomonas* spp.
		- CO<sub>2</sub> --> glyceraldehyde 3-phosphate
	- Modified reverse TCA cycle
		- *Chlorobium* spp.
		- CO<sub>2</sub> --> acetyl-CoA
		- Electron source is S<sup>2-</sup>
	- Hydroxypropionate pathway
		- *Chloroflexus* spp
		- CO<sub>2</sub> -- > glyoxylate
	- 3-Hydroxypropionate/4-hydroxybutyrate Cycle
		- Occurs in some chemolithoautotrophic Archaea
		- Key enzymes
			- Acetyl-CoA Carboxylase
			- Propionyl-CoA Carboxylase
		- Hydroxyburyrate is an intermediate
		- Apparently common in ocean
<br></br>
Heterotrophy
-------------------------

- Respiration
	- electron from organic compounds are transferred to electron acceptor (usually oxygen)
	- Carbon is oxidized to CO<sub>2</sub>
- Fermentation
	- organic substrate is split into at least two parts
		- one part is oxidized
		- Electrons are transferred to other component; becomes reduced
	- Anaerobic process
<br></br>
- Rates of heterotrophic decomposition depend upon the strucutre of the organic substrate

- Diverse heterotrophs consume organic compounds and release CO<sub>2</sub>

- Light-assisted Heterotrophy (photoheterotrophy)
	- Light provides energy for ATP synthess, organics provide C and electron
	- **Purple non-sulfur bacteria**
	<br></br>
	- Anaerobic anoxygenic photoheterotrophs
		- *Erythromicrobium hydrolyticum*, *Sandaracinobacter sibirius* use BChl *a* in obligately aerobic light harvesting to increase catabolic efficiency by 30%
		<br></br>
	- Bacteriorhodopsin mediated photophosphorilation
		- Some Archaea (*Halobacterium halobium*) use bacteriorhodopsin to directly generate proton gradient and ATP synthesis

Methanogens
-------------------------

- All known are Archaea and strict anaerobes

- Very limited choice of substrates
	- Usually grow in symbiosis
<br></br>
- Most use CO<sub>2</sub> and H<sub>2</sub> and are therefore autotrophs
	- Some use small organics like acetate and methanol
		- *Methanosarcina* spp.
<br></br>
- Have unique electron carriers including **methanofuran**, **methanopterin**, and **Coenzymes M** and **F**<sub>420</sub>

Methanotrophs
-------------------------

- Strict aerobes

- Use methane as source of energy and source of electrons by oxidizing it to CO<sub>2</sub>
	- Key enzyme is **methane monooxygenase**
	- CH<sub>4</sub> + O<sub>2</sub> --> CH<sub>3</sub>OH -- > CH<sub>2</sub>O -- > HCOO<sup>-</sup> --> CO<sub>2</sub>
<br></br>
-  Not autotrophs as they use formaldehyde as source of carbon for biosynthesis and growth
	- **Type I** use **ribulose monophosphate** pathway
	- **Type II** use **serine pathway**

Trophic Pyramids
-------------------------

- No process is 100% efficient

Dry weight (g/m2)

10^0    Tertiary Consumers
10^1    Secondary Consumers
10^2    Primary Consumers
10^3    Producers



- Metabolic efficiency of consumers is ~ 10% 

Oct 7
=========================

Global Warming
-------------------------
- CO<sub>2</sub> in atmosphere has been increasing
	- Over past century 0.026 --> 0.038%
	- Causing global warming by greenhouse effect
	- Global warming affects climate, weather patterns, agriculture, health, land use, etc
<br></br>
- Microbes can influence [CO<sub>2</sub>]
	- sequestering through primary productivity
	- Releasing GHGs via decomposition
<br></br>
- Methane is 25x potent as CO<sub>2</sub>
	- [Methane] also increasing
	- Main sources are biological (85%)
	- ~1/2 of methane produced by methanogens is oxidized before reaching the atmosphere
	- Wetlands, rice paddies, ruminants, and termites are largest sources
	- Oceans and landfills are minor sources
	- Total is **300-700 Pg/year**
	- Methane hydrates
		- Forms "ice" under high pressure, low temperature

Midterm Material Ends Here
-------------------------

Nitrogen Cycle
-------------------------

- Atmospheric molecular nitrogen is largest reservoir

- Molecular nitrogen dissolved in water is second largest source

- Terrestrial lithosphere nitrogen is mostly NH<sub>4</sub> and organic compounds

- Terrestrial and aquatic biospheres contain primarily organic nitrogen

- Flux between reservoirs
	- Main source is N<sub>2</sub> in atmosphere, rocks, and oceans
	- Very little atmospheric nitrogen is cyclced because N<sub>2</sub> is extremely stable due to triple bond
	- Small amounts of inorganic nitrogen in water and soil, but actively cycled
	- Biosphere is limited by conversion of inert nitrogen into usable form
		- Needed for nucleic acid and protein synthesis

Nitrogen Fixation
-------------------------

- N<sub>2</sub> --> NH<sub>3</sub>
	- Limiting step for providing usable nitrogen to biosphere
	- Reduction requires electrons and energy
		- Breaking triple bond is energy demanding
	- **N<sub>2</sub> + 8H+ + 8e- + 16ATP --> 2NH<sub>3</sub> + H<sub>2</sub> + 16 ADP + 16 Pi**
	- Electrons come from pyruvate via ferrodoxin or flavodoxin
	- Some electrons go towards producing H<sub>2</sub>
<br></br>
- Key enzyme is **nitrogenase**
	- Irreversibly inactivated by O<sub>2</sub>
<br></br>
- Nitrogen-fixing microbes
	- Free-living cyanobacteria like *Anabaena*
	- Free-living aerobes like *Azotobacter*
	- Free-living anaerobes like *Clostridium*
	- Symbiotic nitrogen fixers like *Rhizobium*
	- Archaea like methanogens
	- Many others
	- No known Eukaryotes may fix nitrogen
<br></br>
- Aerobes have evolved different strategies to protect nitrogenase from O<sub>2</sub> that they need for respiration
	- Rapid oxygen removal via fast respiration
	- Slime layers to slow diffusion of oxygen
	- Complexing oxygen with a special protein
	- Compartmentalizing the nitrogenase in a 

Oct 9
=========================
- Akinete is metabolically inactive resting cell

- Heterocyst
	- Produced by *Anabaena*
	- Protects nitrogenase from oxygen
		- Outer fibrous layer
		- Homogeneous layer
		- Inner laminated layer
	- Microplasmadesmata feeds nitrogen from active cells to heterocysts
	- N<sub>2</sub> --> NH<sub>3</sub> --> Gln --> GOGAT
	- GOGAT
		- Glutamine Oxoglutarate Aminotransferase
		- Glutamine to Glutarate
<br></br>
- Ammonium assimilation
	- Converting NH<sub>3</sub> to organic nitrogen
	- NH<sub>3</sub> is intermediate of nitrogen fixation
		- Also direct from environment
		- At pH 7, exists as NH<sub>4</sub>+
		- Usually incorporated as glutamate or glutamine, which donate amines to amino acids and nucleotides
<br></br>
- Ammonification
	- Converting organic nitrogen to NH<sub>3</sub>
	- Most microbes do this
	- NH<sub>3</sub> 
		- Used for biosynthesis
		- Stable under anoxic conditions
		- Unstable in oxic environments
		- Mostly protonated at neutral pH
		- Binds negatively charged clay particles
		- Taken up by plants or microbes
<br></br>
- Nitrification
	- Oxidation of NH<sub>3</sub> --> NO<sub>3</sub><sup>-</sup>
	- Step 1: Ammonia to nitrite
		- Nitrosofiers like *Nitrosomonas* spp.
		- Ammonia monooxygenase
			- NH<sub>3</sub> + O<sub>2</sub> + 2H+ + 2e- --> NH<sub>2</sub>OH + H<sub>2</sub>O
			- NH<sub>2</sub>OH + H<sub>2</sub>O --> HNO<sub>2</sub> + 4H+ + 4e-
		- Aerobes, transfer e- to O<sub>2</sub>
		- Slow growers; little energy derived
		- Chemolithoautotrophs
		- Nitrite is toxic
		- **Nitrapyrin** inhibits ammonia monooxygenase
	- Step 2: Nitrite to nitrate
		- True nitrifiers
			- *Nitrobacter* spp.
		- Nitrite oxidase
		- NO<sub>2</sub><sup>-</sup> + H<sub>2</sub>O --> NO<sub>3</sub><sup>-</sup> + 2H+ + 2e-
		- Slow growing aerobes
		- Chemolithoautotrophs, heterotrophs
<br></br>
- Denitrification
	- Reducing NO<sub>3</sub><sup>-</sup> to N<sub>2</sub>
	- Dissimilatory nitrate reduction (anaerobic respiration)
	- Consumes a useful nitrogen source
		- Detrimental in soil
		- Beneficial in wastewater treatment
	- Produces gases
		- N<sub>2</sub>, N<sub>2</sub>O, NO)
	- Anaerobes, facultative anaerobes
		- *E. coli* reduce NO<sub>3</sub><sup>-</sup> to NO<sub>2</sub><sup>-</sup>
		- *Pseudomonas* spp. reduce NO<sub>3</sub><sup>-</sup> to N<sub>2</sub>
	- 2NO<sub>3</sub><sup>-</sup> + 5H<sub>2</sub> + 10e- + 12H+ --> N<sub>2</sub> + 6H<sub>2</sub>O
		- Nitrate reductase (synthesis repressed by O<sub>2</sub>)
			- Using oxygen is more efficient, so no need for nitrate
		- Nitrite reductase
<br></br>
- Nitrate Ammonification
	- Reduction of NO<sub>3</sub><sup>-</sup> to NH<sub>3</sub>
	- Not actually denitrification
	- Anaerobes
	- Carbon-rich environments
		- Rumen
	- NH<sub>3</sub> accumulates, not just for assimilation
<br></br>
- Anammox
	- Anaerobic Ammonia Oxidation
	- Performed only by members of *Planctomyces* phylum
	- Form of anaerobic respiration
		- NH<sub>3</sub> is energy source
		- NO<sub>2</sub><sup>-</sup> is electron accceptor
		- N<sub>2</sub> end product
			- One N for NH<sub>3</sub>
			- One N for NO<sub>2</sub><sup>-</sup>
<br></br>
- Water quality
	- Nitrogen is usually a limiting nutrient in soil
		- Fertilizer or microbes is added to supply NH<sub>3</sub>
		- Excess of usable nitrogen allows growth of microbes in water
		- Leads to lack of oxygen
	- Sewage treatment plants try to remove NH<sub>3</sub> and NO<sub>3</sub><sup>-</sup>
	- Nitrite is toxic
		- Blue baby syndrome
	- N<sub>2</sub>O (nitrification and denitrification intermediate) is a GHG and destroys ozone

Oct 11
=========================

Sulfur Cycle
-------------------------

- Abundance
	- Lithosphere
		- Sulfate materials like gypsum(CaSO<sub>4</sub>)
		- Reduced forms like iron sulfide (FeS<sub>2</sub>)
	- Water
		- Dissolved SO<sub>4</sub><sup>2-</sup>
		- Organics
	- Atmosphere
		- SO<sub>2</sub>
	- Biosphere
		- Organics

- Fluxes
	- Lithosphere sulfur turned over slowly
		- Very stable
	- Ocean sulfate slowly recycled
	- H<sub>2</sub>S enters atmosphere from metabolism and SO<sub>2</sub> from fossil fuels
		- returns as sulfurous acid
	- Biosphere requires sulfur for protein and coenzymes
		- Rarely limiting nutrient

- Sulfate reduction
	- Converting SO<sub>4</sub><sup>2-</sup> to H<sub>2</sub>S
	- Dissimilatory sulfate reduction
	- Anaerobic respiration
		- SO<sub>4</sub><sup>2-</sup> + 8H+ + 8e- --> H<sub>2</sub>S + 2H<sub>2</sub>O + 2OH
	- Widespread in anoxic environments
	- Usually limited by supply of electrons
		- Organic substrates for heterotrophs
		- Hydrogen for autotrophs
	- Limited if better e- acceptors present (O<sub>2</sub>, NO<sub>3</sub><sup>-</sup>)
	- Many can use thiosulfate, but not elemental sulfur
	<br></br>
	- Sulfate reducing microbes
		- Mesophilic Gram-negative Delta-proteobacteria
			- *Desulfovibrio* spp.
		- Mesophilic/thermophilic Gram-positive bacteria
			- *dulsulfotomaculum* spp.
		- Thermophilic Gram-negative bacteria
			- *Thermodesulfobacterium* spp.
		- Hyperthermophilic Archaea
			- *Archaeoglobus* spp.
<br></br>
- Sulfur reduction
	- S to H<sub>2</sub>S
	- Anaerobic respiration
	- Mesophilic bacteria like *Desulfuromonas* spp.
	- Hyperthermophilic Archaea like *Pyrodictium* spp.
	- S + 2H+ + 2e- --> H<sub>2</sub>S
	- Elemental sulfur is sold and insoluble in water
<br></br>
- Sulfur oxidation
	- S to SO<sub>4</sub><sup>-</sup>
	- S + 1.5O<sub>2</sub> + H2O --> H<sup>2</sup>SO<sub>4</sub>
	- Example of chemolithoautotrophy
	- Common wherever S and O<sub>2</sub> are available
		- Performed by many microbes
		- Aerobic and anaerobic
		- All temperature ranges
		- Many environments
<br></br>
- Sulfide oxidation
	- H</<sub>2</sub>S to S or SO<sub>4</sub><sup>2-</sup>
		- H<sub>2</sub>S + 0.5O<sub>2</sub> --> S + H<sub>2</sub>O
		- H<sub>2</sub>S + 2O<sub>2</sub> --> H<sub>2</sub>SO<sub>4</sub>
	- O<sub>2</sub> is most common electron acceptor, also NO<sub>3</sub><sup>-</sup>
	- H<sub>2</sub>S is spontaneously oxidized by O<sub>2</sub> and neutral pH
		- Acidic pH keeps sulfide stable
	- Many microbes can perform sulfide oxidation
		- Phototrophs and chemotrophs
		- Aerobes and anaerobes
		- Autotrophs and heterotrophs
		<br></br>
	- Sulfide oxidizers
		- **Anaerobes at neutral pH**
			- Colourless sulfur bacteria like *Beggiatoa*, *Thioploca*, and *Thiomargarita* spp.
				- Filamentous or large
				- Chemolithoautotrophs
				- Live along H<sub>2</sub>S/O<sub>2</sub> gradient
				- Require high [nitrate]
				<br></br>
				- *Thioploca* vertical migration in lakes and intertidal sediments
					- Live at aerobic/anaerobic interface
					- Glides up into aerobic sediments to accumulate nitrate
					- Glides back down to anaerobic zone to oxidize sulfide with stored nitrate

Oct 16
=========================

- *Thiomargarita namibiensis* is largest known bacterium
	- 0.5 mm 
	- 3 million times larger than average bacterial cell
<br></br>
- *Beggiatoa* forms intracellular S granules
	- Forms mats on sea floor where H<sub>2</sub>S seeps
<br></br>
- Sulfide oxidizing microbes con't
	- **Aerobes, acid pH**
		- Chemolithoautotrophs like *Thiobacillus* spp.
			- *T. thiooxidans* use O<sub>2</sub>, produce H<sub>2</sub>SO<sub>4</sub>
			- *T. ferrooxidans* use O<sub>2</sub>, produce H<sub>2</sub>SO<sub>4</sub>
			- *T. denitrificans* use NO<sub>3</sub><sup>-</sup>, does not produce acid
		- Hyperthermophilic Archaea like *Sulfolobus*
		- Rio Tinto, Spain
			- Saturated with S, H<sub>2</sub>SO<sub>4</sub>
			- Sulfide oxidation produces H<sub>2</sub>SO<sub>4</sub>
		- Cueve de Villa Liz, Mexico
			- H<sub>2</sub>S spring in cave
			- 'Snottites' prouce H<sub>2</sub>SO<sub>4</sub>
	- **Anaerobes (phototrophs)**
		- Some deposit S intracellularly or extracellularly
		- some can later oxidize S further
		- Use sulfide as electron source
		- Different tolerances for sulfide
			- Purple sulfur bacteria
				- *Chromatium* and *Ectothiorhodospira*
				- High sulfur tolerance
				- Store S internally
			- Purple non-sulfur
				- *Rhodopseudomonas*
			- Green sulfur
				- *Chlorobium*
				- Store sulfur extracellularly
			- Green non-sulfur
				- *Chloroflexus*
	- **Cyanobacteria**
		- Some like *Oscillatoria* that are normally oxygenic also oxidize sulfide to sulfur
		- Store sulfur extracellularly
<br></br>
- Sulfur assimilation
	- Most microbes use SO<sub>4</sub><sup>2-</sup> as biosythetic S source
	- Internally reduce it to sulfide (requires ATP)
	- Reacts with serine to form cysteine
	- Rarely a limiting nutrient
	- Many organosulfur compounds can be formed
		- Proteins
		- Coenzyme A
		- Vitamins
<br></br>
- Marine organosulfur compounds
	- Marine algae have large amounts of compatible solute to combat osmotic pressure of salinity
	- Sulfur enters atmosphere as dimethyl sulfide (DMS)
		- Volatile
		- May be substrate for methanogens, releasing H<sub>2</sub>S and CH<sub>4</sub>
		- Usually used as electron donor
		- Oxidized to DMSO
			- Used as electron acceptor and reduced back to DMS
			- Seed for cloud formation
	- Dimethylsulfonium propionate (DMSP)
		- Can be degraded as carbon and energy source
		- Process releases DMS
<br></br>
- Issues of sulfur cycle
	- Microbial diversity
		- Organisms that oxidize sulfide are diverse in phylogeny, morphology, physiology, habitat
	- Marine sulfur cycle
		- DMS and H<sub>2</sub>S enter atmosphere
			- H<sub>2</sub>S is toxic and corrosive
			- Contributes to acid rain
				- H<sub>2</sub>SO<sub>3</sub>
		- Sulfate and DMSO are seeds for cloud formation
	- Acidification
		- S and sulfide oxidation produces sulfuric acid (H<sub>2</sub>SO<sub>4</sub>)

Oct 18
=========================

- Phosphorus Cycle
	- Primarily concerned with acquiring PO<sub>4</sub><sup>3-</sup>
	- Few microbes catalyze reductions or oxidations of P between PO<sub>4</sub><sup>3-</sup> and phosphine (PH<sub>3</sub>)
		- *Bacillus caldolyticus*: hydrophosphite to phosphate
	- PO<sub>4</sub><sup>3-</sup> needed for nucleic acids, ATP, phospholipids
	- Often a limiting nutrient
		- Especially in aquatic environments
	- Microbes are highly efficient and phosphate uptake
		- Some store it as insoluble polyphosphate
	- Insoluble inorganic phosphate (Calcium phosphate) are unavailable
	- In oxic environments, PO<sub>4</sub><sup>3-</sup> binds iron
	- More available PO<sub>4</sub><sup>3-</sup> in anoxic environments
	- Phosphorus containing fertilizers can trigger algal blooms
		- *Microcystis* produces microcystin, a hepatotoxin
<br></br>
- Iron cycle
	- **Reservoirs**
		- 4<sup>th</sup> most abundant element in lithosphere
		- No free iron in water bodies
		- None in atmosphere
		- Insignificant amount in biosphere
	- **Fluxes**
		- Very small
		- Microbes *slowly* liberate iron from rocks
	- Aerobes need iron
		- Cytochromes
		- Iron-sulfur proteins
	- Concentrations in environment are low
		- 10<sup>-6</sup> in anoxic environments
		- Less (10<sup>-9</sup>) if carbonate or sulfide concentrations are high
		- Oxically at neutral pH
			- Fe<sup>3+</sup> is 10<sup>-18</sup>M
			- Fe<sup>3+</sup> precipitates as Fe(OH)<sub>3</sub> and FeO(OH)
		- Oxically, low pH
			- Fe<sup>3+</sup> and Fe<sup>2+</sup> are both available
	- Siderophores assist in scavenging iron
		- Improves transmembrane transport
		- **Ferric reductase** removes iron from siderophore
	- Magnetite formation
		- *Magnetospirillum* spp.
		- *Magnetobacterium* spp.
		- Precipitate magnetite crystals (Fe<sub>3</sub>O<sub>4</sub>)
			- Crystals inside membrane
			- Align in a row called magnetosome
			- Provides direction for motility
				- Magnetotaxis
				- Orient themselves to nearest pole
				- Allows swimming up or down to reach microaerophilic environments
				- Magnetite remains after cell death
					- Considered as evidence of life
	- Iron reduction
		- Converting Fe<sub>3+</sub> to Fe<sub>2+</sub>
		- Can be reduced abiotically by sulfide
		- Microbes reduce in a form of anaerobic respiration
		- Widespread process in anoxic terrestrial environments
			- *Shewanella* spp.
			- *Geobacter* spp.
		- Use e- from organics or H<sub>2</sub>
		- Microbes in marine sediments can generate electricity
			- Pure cultures in lab can generate electricity from sugars
				- *Synechocystis* can transfer electrons along nanowire directly to iron globules
				- geopilin

Oct 23
=========================

Iron Oxidation
-------------------------

- Process of converting Fe<sup>2+</sup> to Fe<sup>3+</sup>
	- Occurs spontaneously with O<sub>2</sub> at neutral pH
	- Forms rust-coloured ferric iron precipitates
	- Found naturally at iron seeps 
- At interface of Fe and O<sub>2</sub> gradients, *Gallionella* uses Fe<sup>2+</sup> as e- donor and O<sub>2</sub> as e- acceptor
	- Produces FeO(OH) as reddish-brown stalks
	- Chemolithoautotrophs (Calvin cycle)
<br></br>
- At acidic pH (mine sites)
	- Pyrite (FeS<sub>2</sub>) is exposed to O<sub>2</sub>
	- *Thiobacillus ferrooxidans*
		- Chemolithoautotrophic, acidophilic
	- Rusticyanin is used to extract electrons for electron transport chain for energy
		- Rusticyanin transfers electrons from iron into the cell via cytochrome c
		- O<sub>2</sub> is reduced by electrons from cytochrome c; cyt c pumps protons out of cell (pH of periplasm = 2.0)
		- ATP synthase is driven by large H+ gradient
	<br></br>
	- Fe<sup>3+</sup> is produced
		- Precipitate as iron hydroxide
		- Exothermic process; temperatures can exceed 60 C
			- Mesophiles are replaced by thermophilic acidophiles like *Sulfolobus*
	<br></br>
- Issues of iron oxidation
	- Acid mine drainage
		- Mining exposes pyrite to oxygen
			- pyrite is oxidized
			- Sulfuric acid is released
				 - Encourages *T. ferrooxidans*, which produce more acid
				 - Positive feedback loop

Biogeochemical cycling review
-------------------------

- Most processes are carried out only by Bacteria and Archaeoglobus
	- Chemolithoautotrophy
	- Anaerobic respiration
	- Nitrogen fixation
	- Methanogenesis and methanotrophy
<br></br>
- Some processes are required for biosynthesis
	- CO<sub>2</sub> fixation
	- nitrogen fixation
	- nitrogen an sulfur assimilation
	- Require electrons and ATP
	- Microbes scavenge trace amounts of Fe, P
<br></br>
	- Many of the processes are used for energy production, often using redox
		- Reduced molecules are oxidized
			- CH<sub>4</sub>, NH<sub>3</sub>, S<sup>2-</sup>, S, Fe(II), H<sub>2</sub>
			- Used as source of electrons
			- Often performed by chemolithoautotrophs
			- Oxygen or nitrate used as electron acceptor
			- Energy comes from electron transport chain
		<br></br>
		- Oxidized molecules are reduced
			- CO<sub>2</sub>, NO<sub>3</sub><sup>-</sup>, SO<sub>4</sub><sup>2-</sup>, Fe(III), and O<sub>2</sub>
			- Used as terminal electron acceptors
			- Energy from electron transport chain
<br></br>
- Microbes role
	- Accelerate chemical reactions
	- Affect redox state of most elements
		- C, N, S, Fe in particular
	- Accelerate flux of elements between reservoirs
	- Control chemistry of microenvironments, habitats, and whole Earth


Oct 25
=========================

Habitats
-------------------------

- Terrestrial
	- Categorized by Depth
		- Surface Soils
		- Vadose zone
			- Unsaturated
		- Saturated zone
			- Aquifers
				- Shallow
				- Intermediate
				- Deep
				- Deeper aquifers flow slower, is more anoxic, less connected to surface and other cycles
		- Diversity decreases with depth
<br></br>
	- **Soil**
		- Properties:
			- Porosity
				- Allows movement of air, water, and microbes
				- Distance between particles is micrometres to millimetres
				- Distances inside particles ranges from nanometres to micrometres
			- Moisture
				- Most important variable
				- Affects physical structure, chemical composition, gas diffusion, microbial survival and activity
				- Microbes adapt to intermittent dryness (spores, cysts)
			- Texture
				- Dependant upon composition
					- Clay, silt, sand
			- Particle Surface Area
				- Increases water adsorption
				- Substratum for microbial growth
					- sand particles are 50-2000 micrometres --> 10 cm<sup>2</sup>/g
					- silt particles are 2-50 micrometres --> 500 cm<sup>2</sup>/g
					- clay particles are <2 micrometres --> 8 000 000 cm<sup>2</sup>/g
						- Clay is negatively charged
			- Organic content
				- Most soils are mineral soils
					- Formed by weathering of rocks
					- Limited in organic carbon
				- Organic soils (bogs and marshes)
					- Have abundant organic carbon
					- Formed from sedimenting plant matter
					- If wet, may be anaerobic
						- If anaerobic, may form methane, sulfide, and acid
				- Humeric material (humus)
					- Decaying plant material becomes unrecognizable
					- Easily degraded compounds are gone
					- Variable chemical composition
					- Complex, highly cross-linked polymers
					- Spongy structure that absorbs water, ion, and organics
			- Nutrients (other than C)
				- All highly variable
				- O<sub>2</sub> supply limited in water and wet soils
				- N, P often limiting nutrients
					- Delivered by water
					- Many soil microbes fix N<sub>2</sub>
					- Some microbes increase weathering of P from rocks
			- Microenvironment
				- Microhabitats for soil microbes
				- Interior and exterior surfaces of soil aggregates
				- Pores generally have less oxygen than atmosphere due to respiration
					- May also have methane or sulfide
			<br></br>
		- **Soil Horizons**
			- Ordered O, A, E, B, C, R
			- Layers vary in size from location to location
				- May be totally absent
		<br></br>
		- Communities
			- Culturable microbes reach 10<sup>9</sup> per gram dry soil
			- Over 10000 species of bacteria, many species of fungi, algae, protozoa
				- Estimates; real numbers may be higher (VBNC)
			- Distribution is patchy
			- Stable communities of **autochthonous** (indigenous) form
				- Short term populations of **allochthonous** microbes may form
			- Microbes usually attached to surfaces
				- Only 10% are free-living
			- Greatest diveresity found in rhizosphere
<br></br>
	- **Rhizosphere**
		- Region of soil directly influenced by plant roots
		- Includes rhizoplane, the actual surface of plant roots
		- Interactions between microbes and roots:
			- Water uptake by plants and microbes
			- Release of organics into soil
			- Growth factors and substrates
			- Availability of mineral nutrients
		<br></br>
		- Plants help microbes
			- Plants release exudates
				- amino acids
				- vitamins
				- tannins
				- sugars
				- complex carbohydrates
				- Tends to select from Gram-negatives
					- Motile rods
					- *Pseudomonas* spp.
		- Microbes help the plant
			- Increased recycling and solubilization of mineral nutrients
			- Synthesis of vitamins, amino acids, auxins, cytokinins, gibberellins
				- Stimulate plant growth
			- Antagonism of plant pathogens

Oct 28
=========================

- Subsurface
	- Active subsurface microbes exist
	- Physical environment is very stable
		- Typically anoxic
		- Rock is often layered (shales & sandstones)
		- Substrates are often limiting
			- Pristine environment selects for oligotrophs
		- Most microbes are associated with solid surfaces
		- Microbial transport is mostly from water flow
		- Inorganic nutrients delivered by slow underground flow of water through rock pores
	- Electron acceptors are consumed according to thermodynamic prinicples
		- O<sub>2</sub>, NO<sub>3</sub><sup>-</sup>, Fe<sup>3+</sup>, SO<sub>4</sub><sup>2+</sup>, CO<sub>2</sub>
<br></br>
- Deep subsurface
	- Substrates
		- Very slow degradation of humic  material
		- Water can react with some basalt rocks to form H<sub>2</sub>
<br></br>
- Petroleum reservoirs
	- Carbon abundant in complex molecules
	- Water and nutrients are limiting
	- Amount and quality of petroleum in subsurface depends on how much it has been biodegraded
	- Sulfate-reducing bacteria that degrade hydrocarbons generate sulfide that "sours" oil
		- *Archaeoglobus* spp. responsible at high temperatures
		- Seawater pumped into well is sulfate source
<br></br>
- Deep hot biosphere
	- Microbes have been found for as deep as researchers have drilled (3 km in basalt)
	- Temperature increases with depth
		- Microbes isolated from formations at 80 C
	- Microbes also grow in subsurface under ocean
		- Sequences of thermophilic Archaea and Bacteria found in fluid from ocean crust (3.5 million years old, 65 C, 300m)
	- Even though microbial numbers are low per gram, vast area means it accounts for 1/3 of total global biomass
<br></br>
- Deserts
	- Microbes grow on and inside rocks
	- Microbes form **biological soil crusts** which stabilize soils
	- Of particular interest:
		- McMurdo Valley in Antarctica
			- Ross Desert
			- Porous sandstone
			- Fungi, algae, cyanobacteria, bacteria grow as cryptoendoliths inside rocks
			- Tolerant of cold, dry
			- Only active a few weeks per year
			- Dominated by lichens or cyanobacteria that colour the rock
				- Black areas of fungi
				- Green areas of algae or cyanobacteria
				- Iron is leached from rocks by oxalic acid; rocks turn red or white
		- Atacama Desert, Chile
			- The driest place on Earth
				- Average annual rainfall <0.01 cm/year
				- Some places have not seen rain for hundreds of years
				- 0-25 C
				- **Nitrates actually accumulate**
			- Many sampling methods detect no life in the driest parts of the desert
				- Used to test detection methods for Mars rovers
			- Rare example of a place with only intermittent life

Oct 30
=========================

Aquatic Habitats
-------------------------

- Hydrosphere
	- Freshwater is studied by limnology
	- Marine studied by oceanography
<br></br>
- Lakes
	- Contain relatively little water (0.01% of water)
	- Parameters:
		- **Light penetration**
			- Insensity decreases with depth
			- Short wavelengths penetrate deeper
			- Zones:
				- Euphotic zone at top has light
					- Primary producers are associated with euphotic zone
				- Profundal zone
					- Deeper water beyond the depth of effective light penetration (<1%)
					- Secondary producers
					<br></br>
		- **Depth**
			- Zones
				- Littoral zone
					- Contact with shore
					- Variable
					<br></br>
		- **Temperature**
			- Thermal stratification in summer
				- Warm water is less dense than cold; stays on top
			- Lake turns over in summer
				- Thermocline breaks down and water mixes
			- Zones:
				- Epilimnion
					- Warm, light, high [O<sub>2</sub>]
					- Most primary production
					- Depleted minerals
				- Thermocline
					- Steep temperature gradient
					- Little mixing across boundary
				- Hypolimnion
					- Cold, minimal light, low [O<sub>2</sub>]
					- High mineral concentration
					<br></br>
		- **Microbial populations**
			- Culturable: 
				- 10<sup>2</sup> to 10<sup>5</sup>/ml in water
				- 10<sup>6</sup> in sediment
			- Phototrophs
				- Phytoplankton
					- Algae
					- Cyanobacteria (some have gas vesicles for bouyancy)
				- Purple sulfur, purple non-sulfur, green sulfur *if* bottom of lake becomes anoxic
				- Distribution based on ability to use different wavelengths and tolerate sulfide
			- Heterotrophs found throughout
				- Mostly at thermocline, Neuston, and benthos
				- Often found on detritus particles
			- Anaerobes found in sediment
				- Sulfate-reducers (especially in saline lakes) and methanogens (especially in freshwater)
			- Predatory microbes like protozoa (zooplankton) found throughout lakes
			- Significant input of allochthonous microbes, but they die quickly
			- Microbes are part of food web
			<br></br>
		- **Activities of microbes**
			- Principal ecological function of microbes in lakes:
				- **P**erform mineral cycling
				- **Contribute** to primary production
				- **D**ecompose organic matter
				- **A**ssimilate dissolved organic matter
				- **S**erve as a food source for grazers
	<br></b>
	- Eutrophication
		- Most lakes are oligotrophic
		- Primary producers are limited by mineral nutrients
		- Influx of nitrates and phosphates lead to bloom of phototrophs
		- Phototrophs supply organic carbon to heterotrophs
		- Microbial respiration makes lake anoxic from bottom up
			- Higher organisms die from hypoxia
		- Poor water quality

Nov 1
=========================

- Unusual lakes
	- Meromictic lakes
		- e.g. Mahoney Lake, BC
		- Deep
		- Do not turn over
		- Sediments are not disturbed
			- Preserves historical record
		- Usually saline
		- Some develop visible layer of **purple sulfur bacteria** at sulfide-light interface
	- Salt lakes
		- e.g. Great Salt Lake, Dead Sea
		- Extremely high [NaCl]
		- Alkaline or neutral-
	- Soda lakes
		- e.g. Mono Lake, CA, USA
		- Extremely high [NaHCO<sub>3</sub>]
		- Very high pH (~ 11)
		- Carbonates precipitate to form tufa mounds

- Oceans
	- Covers 70% of Earth's surface
		- 97% of Earth's water
		- Uniform and stable
			- Mixed by tides, currents, and thermohaline circulation
		- Salinty is 3.5 +/- 0.2 % (35g/L NaCl)
		- pH is 8.4 +/- 0.1
		- Temp is 2-4 C except 100m (18 C)
		- Sulfate ~30 mM
		- Trace N, P, Fe
		- O<sub>2</sub> decreases below surface, but penetrates to bottom
		- **Zones**:
			- Light penetration
				- Photic zone (top 30-300 m)
				- Aphotic zone (below photic)
			- Depth
				- Pelagic zone is open water
				- Littoral zone is shore
				- Benthos is bottom
					- Average depth is 3.8 km, maximum in 11 km (Marianas Trench)
				- Sediment
					- Average 30 m deep, accumulation at 1-10 cm per 1000 years
		- Primary production
			- Slow compared to other environments
			- Limited by mineral nutrients like nitrogen and iron
			- Found only near surface (top 50 - 100 m)
			- Highest chlorophyll *a* near shore as measured by satellites
		- Diversity of Microbes
			- Phyotplankton
				- Algae and cyanobacteria
			- Aerobic heterotrophs
				- Mostly sinking with detrital particles
			- Prochlorophytes
				- Small, but numerous (25-50%)
			- Crenarchaeota
				- Most crenarchaeota are hyperthermophiles, but ocean-dwellers are not
			- Top of sediments are aerobic, but deeper zones are anaerobic
				- Sulfate reducers and methanogens are supported to great depths
	- Deep Ocean
		- Deeper than 1 km (70% of ocean volume)
		- *Very* oligotrophic
			- No light for phototrophs
			- Most detrital particles are consumed before they reach the deep ocean
		- Cold, at high pressure
			- Hydrostatic pressure increases 1 atm with every 10 m depth
			- Trenches at 10km have 1000 atm
			- Microbes are adapted to pressure and depth
				- Piezophiles and extreme piezophiles
			- Oxygen accumulates at sea floor because there is too little respiration to consume it

Nov 6
=========================

Hydrothermal Vents
-------------------------

- Exception to barren landscape on sea floor

- Two types
	- Warm vents
	- **Black smokers**
		- Black smoke are metal sulfides
		- Outflow is extremely rich in minerals
			- Reduced compounds are electron donors for chemolithoautotrophs
			- Support diverse and productive communities
		- No organics from fluids
		- Chimneys built from precipitated iron sulfides
			- Walls are full of hyperthermophiles
			- Artificial surfaces are colonized by prokaryotes up to 135C
		- **Microbes**
			- "Food" comes in form of reduced elements
				- H<sub>2</sub>, H<sub>2</sub>S, S, CH<sub>4</sub>, NH<sub>4</sub><sup>+</sup>, Fe<sup>2+</sup>
			- Free-living surrounding water, or live in biofilms on chimneys
				- Some for endosymbioses with tube worms
			- Hydrogen consuming methanogens (CO<sub>2</sub>) in seawater
				- closer to source of fluid
				- *Methanothermus*
			- Hydrogen consuming sulfate reducers (SO<sub>4</sub> in seawater)
				- *Thermodesulfobacterium* and *Archeaoglobus*
			- Hydrogen consuming sulfur reducers
				- S accumulates around vents from abiotic sulfide oxidation
				- *Pyrodictium*
			- Hydrogen oxidizing bacteria that use O<sub>2</sub>
				- *Hydrogenomonas*
			- Sulfur and sulfide oxidizers
				- *Beggiatoa* and *Sulfolobus*
			- Methane and carbon monoxide oxidizers
				- *Methylomonas*
			- NH<sub>4</sub><sup>+</sup> oxidizers
				- No known species yet
			- Metal oxidizers
			- Metal reducers (anaerobic respiration)
			<br></br>
			- Photosynthesis
				- Harvest black lody radiation from vent water
				- *Citromicrobium* 

Hot Springs
-------------------------
- Water comes out of ground at 40-95C

- Microbes align themselves along temperature gradient
	- Hyperthermophiles at source (>65C)
	- Cyanobacteria form mats at <65C
<br></br>
- Water is usually reduced, often contains sulfide
<br></br>
- Can be freshwater or saline or hypersaline
<br></br>
- Water can be alkaline or acidic
<br></br>
- Often precipitate silica or carbonate to form mounds
- Grand Prismatic Spring
	- Orange is *Chloroflexus*

Nov 8
=========================

Ice Microbiology
-------------------------

- Artic and Antarctic sea ice are very large areas
	- 2/3 of multiyear sea ice is in Arctic
		- Dozens of years old
		- Smooth
		- Average of 2m thick
	- Antarctic sea ice is thinner, rougher, younger
<br></br>
- Microbes usually live at interfaces
	- Surface of ice in spring and summer
	- Coating bottom of ice
	- Inside ice in brine channels (200-400 micrometres wide)
<br></br>
- Microbial activity
	- Psychrophiles are active so long as there is liquid water
	- Ice temperature
		- Constant -2 C near seawater
		- 0 to -30 C  near surface
		- Seawater salts become concentrated in brine channels that remain liquid
		- Microbes respire at -20 C, grow at -17 C
<br></br>
- Microbial diversity
	- Primary producers
		- dominated by diatoms and other eukaryotic algae
	- Heterotrophic bacteria and protozoa
		- Proteobacteria and *Cytophaga*
		- Archae not yet culture, but found using molecular probes
<br></br>
- Lake Vostok
	- Antarctic lake, covered by 4 km of ice
		- Predicted by theory in 19th century
		- Discovered by seismic soundings in the 1960s
		- Lake deliniated by space-based altimetry in 1996 by British and Russian scientists
	- Extremely unusual habitat for microbes
	- Many different morphologies
	- 200 - 1000 cell/ml
	- 16S rDNA sequencing shows alpha- and beta- Proteobacteria, *Cytophaga*, Gram-positives
	- Cultured bacteria from 3593m
	- <sup>14</sup>C-labelled glucose or acetate coverted to <sup>14</sup>CO<sub>2</sub>
		- Demonstrates heterotrophic activity

Nov 13
=========================

Microbial Interactions Within Populations
-------------------------

- Cell associations more common than individual life
- Methods:
	- Incomplete cell seperation (heterocysts)
	- Fimbriae and pili
	- Glued together after cell division (polysaccharides)
- Aggregation has physical benefits
	- Better adherence to solid surfaces
	- less susceptibility to shearing forces in water
	- Trapping organic particles
	- Large aggregates can inhibit predators
- Density dependence of a processes
	- Magnitude of a process influenced by population density
		- Positive dependence
			- Low molecular weight metabolites that diffuse across membranes can be shared
				- Compatible solutes like glycine betaine in hypersaline environments (*Ectothiorhodospira*)
				- Extracellular enzymes like cellulases (*Clostridium*, *Actinomycetes*, *Ruminococcus*)
					- Important in foret floor, anoxic sediments, animal guts
			- Quorum sensing
				- Cells make autoinducing signal compound
				- As density increases, amount of autoinducer reaches critical levels, triggering gene expression
					- *Photobacterium*, *Vibrio fisheri*, and bioluminescence
					- When N-B-ketocaproyl homoserine lactone reaches critical concentration, it reacts with activator protein, and causes expression of *lux* operon
					- Also important for *Agrobacterium tumefaciens* and *Myxococcus xanthus*
			- Complex life cycles
				- Multicell associations are important for exchange of signal compounds and functional differentiation
					- *Myxococcus xanthus*
						- Feed on cellulose, lignin, and dead or dying bacteria
						- Interactions are necessary for substrate solubilization, gliding motility, and formation of fruiting bodies and myxospores
			- Genetic exchange
				- Beneficial attribute can be passed on to other members of a population
					- Possible if selective pressure is high, and microbes are in close contact
						- Transfomation
							- Free DNA taken up from environment
						- Conjugation
							- Transfer of DNA directly from one cell to another
						- Transduction
							- Transfer of bacteriophage

Nov 15
=========================

Microbial Interactions Con't
-------------------------

- Negative Density Dependence

	- Competition for nutrients
		- At high cell densities, microbes have greater competition
		- **Liebig Principle**
			- Growth is limited by the first substrate to be exhausted
		- Microbes can adapt to starvation conditions, but eventually need substrates for cell maintenance
	<br></br>
	- End product accumulation
		- Some metabolic products are detrimental
			- e.g. organic acids from fermentation (lactic acid)
			- Sulfide inhibits even sulfate reducers at high concentration

Interactions Between Populations
-------------------------

- Competition (-,-)
	- Two or more populations compete for the same resource
	- Coexistance is possible in complex systems, but exclusion is the norm for simple systems
	- Key advantages of a successful strain:
		- High growth rate
		- High affinity for substrate
		- High affinity for the habitat
<br></br>
- Antagonism (-,0)
	- One population is harmed, while the other is unaffected
<br></br>
- Amensalism (-,0)
	- One population secretes substances inhibitory to other populations
		- Ethanol, sulfuric acid, lactic acid, antibiotics, bacteriocins
			- Bacteriocins are proteins lethal to cells highly similar, but not identical, to the secreting cell
<br></br>
- Symbiosis
	- State of species living in close association
		- May be (+,+), (+,0), or (+,-)
		- One species must benefit
	- Free availability of a resource outside symbiosis destabilizes symbiosis
		- In moist habitats, fungi become parasitic upon algae in lichens
		- High nitrogen availability causes dinoflagellates to abandon their endosymbiotic cyanobacteria
	- Ability to provide a limiting resource stabilizes symbiosis
<br></br>
- Mutualism (+,+)
	- Interactions benefit both populations
	
	- Each partner receives a different resource or service
	
	- Facultative mutualism (synergism)
		- In some cases, new metabolic abilities arise which are not possible by one population alone
		- If cooperation involves nutrients, this is called **syntrophy**
<br></br>
	- Obligate mutualism
		- Neither partner can survive without the other
			- Coevolution makes morphological and metabolic modifications
<br></br>
	- Magnitude of benefits may also be conditional upon habitat parameters

	- *Chlorochromatium aggregatum*
		- Symbiosis between phototrophic green sulfur bacterium surrounded by heterotrophic bacteria
		- Phototroph may provide acetate, while the heterotroph provides motility
		- Partnership is chemotactic to sulfide, and phototactic to light
		- Found in anaerobic aquatic habitats
		- Example of obligate mutualism
<br></br>
	- Methanogens and syntrophs
		- *Syntrophomonas* produces H<sub>2</sub>
		- *Methanococcus* or *Methanobacterium* consumes the H<sub>2</sub>
		- Methanogen must remove H<sub>2</sub> to make methane
		- Each member gets barely enough energy to survive
		- Together can metablize ethanol, butyrate, etc to increase overall amount of fermentation
		- Very important in anaerobic sediments and animal guts
		- Interspecies H<sub>2</sub> transport is important in all anaerobic environments

Nov 18
=========================

- Organisms only cooperate if there is something to be gained

- Commensalism (+,0)
	- One population benefits, other is unaffected
	- Provider may remove toxic agents, while other grows
		- Aerobic heterotrophs consume O<sub>2</sub>, allowing anaerobes to grow in sediment beneath them
	- Provider may secrete components that serve as carbon and energy sources
		- Methanogens secrete methane, which bubble up where it is consumed by methanotrophs
	- Provider may convert a non-utilizable compound to form that can be used by recipient
		- Methanotrophs can co-metabolize TCE to TCE epoxide, which breaks down chemically to formate and glyoxylate
<br></br>
- Predation and parasitism (+,-)
	- Negative interaction, complex relationships
	- Ectoparasites (external)
		- Myxobacteria eat ead or dying cells by excreting exoenzymes that lyse both Gram-positive and Gram-negative cells
	- Endoparasites (internal)
		- *Bdellovibrio* spp. invade Gram-negative
		- Grows inside cells
		- Can destroy Gram-negative culture in a few hours

- Bacteriophages
	- Important in limiting bacterial population sizes
	- Intracellular parasites and *very* host-specific
	- Some can lay dormant as prophage that are induced by cell damage

Nov 20
=========================

Building Communities
-------------------------

- **Dispersal**
	- Dissemination of organisms between locations
	- Facilitated by special resistant structures (endospores, cysts, conidiospores) or animal behaivours
- **Colonization**
	- Establishment of a population or community in a new habitat
- **Pioneer Organism**
	- First species that colonize a newly formed habitat
		- e.g. recently solidified lava, bedrock following glacier retreat
- **Succession** (Primary and secondary)
	- Change of composition of community over time
	- Habitat change by community
- **Climax community**
	- Species assemblage that has reached equilibrium and no longer changed is composition significantly

Quantifying Richness and Diversity
-------------------------

- **Richness**
	- Number of species in an environment
- **Diversity**
	- Number of species  **and** evenness of their populations
- Used Shannon-Weaver diversity index (H'):
 
```
function shannonWeaverIndex(allSpecies)
	for (species in allSpecies)
		relAbundance = species.total / allSpecies.total
		H += -(relAbundance * ln(relAbundance))
	return H
```
 
Fungal Symbiosis
-------------------------

- Lichens
	- Symbiosis of fungus with phototrophic microbe
	- Grow in difficult, often dry environments
		- Rocks, tombstones, buildings, tree trunks
	- Slow growing
<br></br>
	- Shape is determined mostly by fungus
		- Ascomycetes and some Basidomycetes
		- Heterotrophic fungus plays a structural role
			- Protects phototroph from drying and erosion
			- Facilitates uptake of water and inorganics
			- Produce compatible solutes
<br></br>
	- Algae or cyanobacteria from distinct layers or clumps
		- Species associations are fairly specific
		- Interact in a reproducible way
		- Phototrophs provide organics to both members
		- Cyanobacteria may also fix nitrogen
<br></br>
- Mycorrhizae
	- >5000 species of fungi may form mycorrhizae
		- Unlike most symbioses, not very species specific
<br></br>
	- Symbioses of fungi with plant roots
		- Mutualistic
		- Fungi become integrated into physical structure of roots
		- Fungi stimulate plants to form mycorrhizae by releasing plant growth hormones
	
	- Fungi benefit from constant supply of sugars from plant
	
	- Improves absorption of nutrients like phosphorus
	
	- Mycorrhizal associations lead to greater species diversity in trees
	
	- Can improve pathogen resistance in plants
	
	- **Ectomycorrhizae**
		- Fungus forms an external sheath
		- Fungal hyphae may penetrate the intercellular spaces of epidermis of root
		- No invasion
		- Mainly found in forest trees (boreal and temperate)
<br></br>
	- **Endomycorrhizae**
		- Invade living cells of root
		- Found on ~90% of world plant flora
		- **Vesicular arbuscular mycorrhizae** (VAM) are found on >80% of terrestrial plants

Nov 22
=========================

Bacterial Symbioses with Plants
-------------------------

- Many indirect  interactions of microbes with plant roots and exudates in rhizosphere

- Some symbioses are based on nitrogen fixation
	- *Frankia* association with alder trees
	- Cyanobacteria like *Anabena* with water fern
	- *Rhizobium* with legumes
	<br></br>
	- Actively encouraged by plant to promote growth
<br></br>
- **Legume symbiosis of Rhizobia**
	- Rhizobia include several genera
		- 57 species in 12 genera
	- Associate with specific legumes
		- *Rhizobium* with peas, beans, clover (fast growing)
		- *Bradyrhizobium* with soybeans and broad host specificity (slow growing)
	- Gram-negative rods
	- Aerobes
	- Motile
	- Produce copious amounts of extracellular slime
	- GC = ~60%
	- Metabolize glucose via Entner-Duodoroff pathway
		- Less common in bacteria than glycolysis
	- Fix nitrogen under microaerophilic conditions
	- Some species can grow as hydrogen chemoautotrophs
	<br></br>
	- **Development Process**
		1. Root hairs release flavenoids to attract *Rhizobium*
		2. *Rhizobium* forms infection thread
		3. Infection thread grows into root cortex
		4. Bacteria cells become bacteroids in root cell
		5. Nodule forms
		<br></br>
	- **Chemicals involved**
		- Flavenoids secreted by plants are attractant
		- Rhicadhesin for binding
		- Nod factors to cause curling
		- Cellulosic materia forms thread
		- Peribacteroid membranes and cell differentiation
		- Nod factor stimulates division
		- Leghemoglobin to regulate O<sub>2</sub>
		- Regulated by *nod* and *nif* genes on *Sym* plasmid
		<br></br>
	- **Nitrogen fixation in bacteroid**
		- Symbiosome membrane surrounds bacteroid membrane within the plant cytoplasm
		- Photosynthesis --> sugars --> organic acids --> TCA cycle --> e<sup>-</sup> --> electron transport chain --> ATP --> nitrogenase --> NH<sub>3</sub>
		
Nov 25
=========================

Microbial Associations with Animals
-------------------------

- Ruminants
	- Animals that have a rumen to eat grasses or other cellulose-rich plants
		- Antelopes, cow, sheep, goats, buffalo, deer, moose, caribou, elk, camels, giraffe, hippopotamus, and baleen whales
	- Rumen is extra organ in digestive tract
		- Host microbes that digest cellulose and other plant polysaccharides
		- Microbes provide the products to the animal as **acetate**
		- Rumen provides favourable environment and plentiful supply of substrate
		- Large volume (>100 L)
		- Anoxic (Eh -350 mV)
		- Temperature is ~39 C
		- pH is 6.5 (buffered by bicarbonate from saliva)
		- Mean redidence time is ~12h, then regurgitated as cud
		<br></br>
	- Plant digestion
		- Macrostructure broken down by chewing and mechanical action in rumen
		- Products are further degraded by fermentation in anoxic environment of rumen
		- Cellulolytic bacteria and protozoa hydrolyze celulose to glucose
		- Glucose is fermented to volatile fatty acids and carbon dioxide, and some is converted to methane
		- Lipids are hydrolyzed, glycerol is fermented 
		- Proteins are hydrolyzed and deaminated
		- Lignin is not degraded
		<br></br>
	- Mutualism between animal and microbes
		- Fatty acids (acetate) pass through the rumen wall as main source of energy for animal
		- Microbes also synthesize amino acids and vitamins that are essential for the animal
		- Environment supports rich and diverse community
			- 10<sup>10</sup> - 10<sup>11</sup> bacteria per ml
			- 10<sup>4</sup> - 10<sup>6</sup> anaerobic protozoa per ml
			- 10<sup>4</sup> - 10<sup>6</sup> fungi per ml
		- Microbes themselves, when regurgitated with cud, are also major source of nutrition for animal (carbon and nitrogen source)
		<br></br>
	- Syntrophy and competition within the rumen
		- Cellulose degradation is a multistep, multimicrobe process
		- Microbial populationare very consistent over time and between animals (depending on feed)
			- Grasses contain mostly cellulose
			- Grains contain more starch
			- Hay from legumes have more pectin
			- Rapid changes in diet can disrupt syntrophy, endanger animal
		- Microbes are heterotrophs
			- Mainly copiotrophs
	<br></br>
	- Cellulolytic bacteria
		- Cellulose makes up 40% of food, but cellulolytic bacteria make up only 5% of cells
		- Variety of anaerobic vacteria, fungi, and protozoa compete to break down cellulose and hemicellulose
			- *Ruminococcus*
		- Niche differentiation
			- Microbes differ in substrate affinities, attachment sites
			- Allows more species to coexist
	- Fermenters
		- Covert glucose to acetate
			- *Clostridium*
		- Also produce H<sub>2</sub> and CO<sub>2</sub>
		- Syntrophs convert fatty acids to H<sub>2</sub>
			- Waste
	- Methanogens
		- Use H<sub>2</sub> and acetate
		- Produce methane, which is removed by belching
		- Large source of greenhouse gas
<br></br>
- Non-ruminant herbivores
	- Some grass-eating animals are not ruminants
		- Horses, rabbits
	- Have **cecum** instead
		- Extension of large intestine
		- Post-gastric fermentation
		- Site for cellulolytic fermentation
		- Smaller than rumen
		- Shorter mean residence time
		- Cannot digest microbes
<br></br>
- Termites
	- Three feeding guilds
		- Wood, soil, fungi
	- Digestion is in **paunch** or **hindgut**
		- 1-2 microlitres
		- Long, narrow tube
		- Anoxic (Eh = -200 to -300 mV)
		- Steep oxygen gradient
			- O<sub>2</sub> diffuses from outside, but is consumed immediately
	- Hindgut microbes are densely packed
		- Cilliated an flagellated protists, many bacteria
		- Degrade cellulose and ferment it to acetate, H<sub>2</sub>, and CO<sub>2</sub>
		- Acetogens
		- Methangens use H<sub>2</sub> and CO<sub>2</sub> to release methane
		- Bacteria may also fix nitrogen, or recycle uric acid
		- Microbes in terminates may degrade lignin
			- Second most abundant component in wood
			- Very difficult to degrade 
			- Requires oxygen
			- Only occurs in edge of hindgut where conditions are still aerobic

Nov 27
=========================

- Acetogens
	- Produce acetate
	- Metabolically diverse
	- Glucose --> 2 acetate + 2 CO<sub>2</sub> + H<sub>2</sub>
	- 2 CO<sub>2</sub> + H<sub>2</sub> --> acetate
		- Acetyl-CoA pathway
	- Affinty for H<sub>2</sub> is lower than other microbes
	- Thermodynamically derive less energy than other organisms
		- Usually outcompeted by methanogens, except in termite hindgut
	- e.g. *Clostridium aceticum*
<br></br>
- Mutualism within mutualism
	- Cellulose degradation in hindgut includes protozoa
	- Fermentation in protozoa requires removal of H<sub>2</sub>
		- Methanogens remove H<sub>2</sub>
	- Some protozoa harbour methanogens as endosymbionts
		- Special organelle called **hydrogenosome**
			- Analogous to mitochondria in aerobic protozoa
			- Enzymes oxidize pyruvate to acetate and release hydrogen
			- Methanogens in hydrogenosome scavenge H<sub>2</sub>
			- **Hydrogen consumption makes fermentation possible**
			- Volatile fatty acids like acetate are absorbed through the hindgut wall for use by termite

Mutualism at Deep Ocean Hydrothermal Vents
-------------------------

- *Riftia* tubeworms are classic example
	- *Very* distant relative of earthworms

	- 1-2 m long, 10-15 cm in diameter

	- Grow at high densities and high growth rates around vents

	- Have hard white chitinous sheath for protection

	- Bright red plume is result of unusual haemoglobin variant

	- Structure
		- No mouth, anus, or intestinal tract
		- Lamallae absorb oxygen and sulfide from solution in water
		- Spongy tissue called **trophosome** makes up >50% of body
		- Many sulfur (S<sup>o</sup>) granules in trophosome
<br></br>
	- Endosymbiotic association with chemolithoautotrophic bacteria
		- Similar to marine sulfide oxidizers
		- Sulfide oxidizing bacteria ~ 10<sup>9</sup> cells/g tissue
		- Similar associations found in mussels and clams
<br></br>
	- Nutrition
		- Haemoglobin binds both oxygen and sulfide
		- Bacteria oxidzer sulfide with oxygen as electron acceptor
		- Bacteria fix CO<sub>2</sub> into organic carbon that is either excreted or released from dead bacteria
			- Serves as carbon source for *Riftia*
<br></br>
	- Basis for mutualism
		- Worm provides habitat, sulfide, CO<sub>2</sub>, and O<sub>2</sub>
		- Bacteria carry out novel metabolic functions and provide food

- Other deep ocean animals with symbioses
	- Fish
		- Bioluminescent *Photobacterium* spp.
			- Quorum sensing --> **luciferase** --> light in light organ
		- Fish provides habitat and nutrients
		- Bacteria provide communication
<br></br>
	- Arthropods
		- Cuticle of embryonic shrimp (*Palaemon marcodactylus*) are colonized by *Alteromonas* spp.
			- Bacteria produce antifungal metabolite **2,3-indolinedione** (isatin)
				- Shrimp are resistant to infection by *Lagenidium callinectes*
			- Shrimp provide habitat
<br></br>
	- Komodo Dragon (*Varanus komodoensis*)
		- Harbour >54 pathogenic bacteria in saliva, including *E. coli* and *Pasteurella multocida*
		- After lizard bite, sepsis caused by massive infection incapacitates large mammalian prey
		- Bacteria get habitat and nutrients
		- Lizard gets increased hunting efficiency

Nov 29
=========================

Negative Microbial Associations With Plants
-------------------------

- Fungal infections are most important economically
	- "Rots", "smuts", "blights", "rusts"

- Viral diseases cause moderate crop losses
	- Leaf lesions

- Bacterial infectins cause moderate crop losses
	- "Blights", "wilts", "rots", "cankers", "galls"
	- Pathogens have evolved a broad range of pathogenicity and virulence mechanisms that are tightly controlled at the genetic level

- **Crown gall**
	- Tumourous growth
	- Caused by *Agrobacterium tumefaciens*
		- Alpha-proteobacteria
		- Close relative of some rhizobia
	- Mediated by Ti plasmid (200 kb)
		- Induces tumours
		- *vir* genes are virulence factors
		- Transfer (T-DNA)
			- Carries *onc* genes
		- Gene for uptake and catabolism of **opines**
			- Modified amino acids
				- e.g. octopine and nopaline
			- Used as carbon and nitrogen source for *Agrobacterium*
<br></br>
	- Infection process:
		- *Agrobacterium* transferred to plant via wound
		- Wounded plant releases **acetosyringone**
			- Stimulates healing of plant tissue
			- Also chemoattractant for bacteria
		- *Agrobacterium* attach to pectin at wound using glucans
		- *Agrobacterium* cells make cellulose microfibrils to trap aggregate of cells on surface
			- Mediated by quorum sensing
		- Phenolic signals from wounded plant induce expression of *vir* genes
		- Products of *vir* genes Ti plasmid and transport T-DNA into plant cell (like conjugation)
		- T-DNA inserts randomly into plant genome
			- Transformed plants will express inserted genes even in absence of bacteria
<br></br>
	- Modified Plant Metabolism
		- T-DNA carries genes for tumour formation (*onc*)
			- Genes for plant growth hormones
				- auxins
				- cytokinins
		- Carries genes for making opines
			- *ops* genes
<br></br>
	- Genetic Engineering vector
		- Can use Ti plasmid system of *Agrobacterium* to transfect plant cells
			- e.g. luciferase inserted into T-DNA will cause plant to exhibit bioluminesence
			- Herbicide resistance
			- Pathogen resistance
			- Improved nutritional value
			- Resistance to drought, salinty, etc

Applied Microbial Ecology
-------------------------

- Wastewater treatment

- Biodegradation and landfills

- Bioremediation of organic xenobiotics

- Bioremediation of heavy metals