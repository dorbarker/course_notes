Sept 6
========

Introduction to Mathematical Concepts
-------------------------

Representation of Physical Quantities

- Algebraic product of a number and a unit
- Units combine algebraically
- Course covers 5 of 7 SI fundamental units
	- **time** = seconds (s) -> 9192631770 cycles in Cs-133 nucleus
	- **distance** = metre (m) -> distance travelled by light in a vacuum in 1/299797458 s
	- **mass** = kilogram (kg) -> mass of platinum/iridium artifact in a Parisian vault
	- **mole** (mol) -> the number of C-12 atoms in 12 g of C-12
	- **temperature** = kelvin (K) -> defined such that the triple point of water is 273.16 K

Sept 11
====================

- Two vectors joined tip-to-tail gives resultant vector R
- Easier is to add component vectors of each vector to get R
- Vector can be expressed as:
	- Magnitude and angle
	- Components
	- Convert between the two by Pythagoras Theorem

Kinematics
-------------------------

- In a medium devoid of resistance, all bodies fall at the same speed
- During equal time intervals, a falling object receives equal increases in speed
- Complete description gives position as a function of time

Sept 13
===============

- Constant acceleration forms a linear graph
- Changing velocity forms a curve
- Negative acceleration could, but does not necessarily mean you are slowing down
	- Could mean acceleration is increasing in the negative direction

- Constant acceleration
	- v = v0 +at
- Position
	- delta-x = v0t + 1/2(v-v0)t
	- delta-x = v0t + 1/2at^2
	- x = x0 + v0t + 1/2at^2

Sept 25
=========================

Force and Gravity
-------------------------

- F = ma

- Galileo
	- inertia (horizontal motion)
	- constant acceleration (vertical motion)
<br></br>
- Descartes & Huygens
	- Conservation of momentum (mass * velocity = constant)
<br></br>
- Kepler & Braha
	- Laws of planetary motion
<br></br>
- **Newton's First Law**
	- A free object moves with constant velocity
	- Inertial reference frame
		- Anything without a force acting upon it
- **Newton's Second Law**
	- F = ma
	- Mass
		- Quantity of matter (determined by a balance)
		- Quantity that resists acceleration (inertial mass)
			- Inversely related to acceleration if force is equal
	- Force
		- Push or Pull
		- Disturbs natural state
			- Causes acceleration
		- Proportional to acceleration

Oct 2 
=========================

- Tension
	- Force exerted by rope
	- for an ideal line, the same force is exerted at both ends
	- objects connected by a taut line have same acceleration
- Pulley
	- changes direction of force
	- Must be taut
	- Simplify problem by reducing to one-dimensional problem

Equilibrium applications
-------------------------

- Equilibrium means zero acceleration
- Balance forces in x and y directions
- Non-equilibrium means non-zero acceleration

Oct 7
=========================

- Satellites in circular orbit
	- Speed and radius
		- F = mv<sup>2</sup>/r
- Synchronous orbit
	- Satellite is stationary over a point on Earth's surface
	- Only possible with an equatorial orbit
	- Conditions:
		- T = 1 sidereal day
			- For Earth, 24h - 4m
		- Above equator
		- r<sup>2/3</sup> =  T*sqrt(GM<sub>E</sub>)/2*pi
		
- Centrifugal force and artificial gravity

Oct 9
=========================

- Conversvation of momentum
	- p = mv
	- kg m/s

- Work is change in energy by application of a force over a distance

- 1 N m = 1 Joule

- W = Fs = Fs*cos(theta)

- W = delta-KE

- KE = 1/2mv<sup>2</sup>

Oct 18
=========================

- Energy is neither created or destoryed
	- Mechanical
	- Heat
	- Chemical
	- Electrical
	- Nuclear

- v<sup>2</sup> = 2gh

- Constant force J = F * delta-t
	- Delta-p = J

- Total internal forces on a system are zero
	- F = delta-P/delta-t

Oct 21
=========================

- Completely inelastic collision
	- v1 = v2 = v'

- Completely elastic collision
	- KE<sub>i</sub> = KE<sub>f</sub>
	- If m1 >> m2, v<sub>m2</sub> = 2*v<sub>m1</sub> + v<sub>m2</sub>

Oct 28
=========================

- Rotational kinematics

- Angular displacement
	- Angle swept out by a line intersecting and perpendicular to the axis of rotation
	- Tangential speed
		- v<sub>t</sub> = r * (2 * pi)/T