#!/bin/bash

pandoc -f markdown -t html -o Biological_Energy_Transduction.html Biological_Energy_Transduction.md
pandoc -f markdown -t html -o Genetics_1.html Genetics_1.md
pandoc -f markdown -t html -o Physics_1.html Physics_1.md
pandoc -f markdown -t html -o Microbial_Communities.html Microbial_Communities.md
pandoc -f markdown -t html -o Comp_sci_2.html Comp_sci_2.md
git add -u
git commit -m "Automated commit by convertAndPush.sh"
git push