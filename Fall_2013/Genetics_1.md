Sept 5
==============

Overview
-------------------------

- Testing is on lecture material and lab book problems
- No tutorial Sept 12

- Inheritance
	- Transmission of traits between generations
	- Occurs via hereditary material (nucleic acids)
	- Cellular inheritance by division

- Chromosome
	- Continuous strand of DNA packaged with proteins
	- Linear in eukaryotes
	- Circular in prokaryotes (usually)

Cell Cycle
-------------------------

- Mitosis (M phase)
	- nuclear division
	- cytokinesis
- Presynthesis Gap (G1)
- Synthesis of DNA (S Phase)
- Post-synthesis Gap (G2)
- G1, S, G1 == Interphase

- DNA Replication
	- Single chromatid duplicates to two identical sister chromatids
	- Chromatids joined by centromere
	- Chromatids diffuse during S Phase

Mitosis
-------------------------

- IPMAT

- **Prophase (paired)** -- chromatids condense, visible under microscope
- **Metaphase** -- chromosomes aligned in middle of cell by spindle apparatus (microtubules) attached at centromeres
- **Anaphase (apart)** -- sister chromatids pulled apart
- **Telophase (two)** -- Chromatids reach opposite poles, cytokinesis divide cell in two

- Progeny are genetically identical to parent cell
- Used for growth, repair in somatic cells

Sept 10
==================
Karyotype
-------------------------
- Visualization of metaphase chromosomes
	- Metaphase because chromosomes are condensed and visible
- Numbered from largest to smallest
- In humans, 22/23 are autosomes
- Sex chromosomes are 23rd pair


Cell Division
-------------------------

- Mitosis
	- Progeny are genetically identical to parents (somatic cells)
- Miosis
	- Germline cells
	- Two sets of chromosomes
		- One maternal
		- One paternal
	- 2N = diploid
	- N = haploid
	- One round of duplication
	- Two rounds of division (Meiosis 1 and meiosis 2)

Meiosis steps
-------------------------
- Produces four genetically unique gametes
- Reduces chromosomes from diploid to haploid
- **Genetic variation produced by independent assortment of chromosomes**
- Recombination results in additional genetic variation
 
- Interphase
	- DNA Replication
- Prophase I
	- Pairs physically interact at synapsis
	- Can exchange genetic material
		- Chiasma (pl. chiasmata) is site of exchange
	- Source of genetic variability
- Metaphase I
	- Homologous pair aligns on metaphase plate
- Anaphase I
	- Homologous pairs seperate
	- Duplicated chromosomes remain together
- Telophase I
	- Two cells each with one member of homologous pair are created via cytokinesis

- Prophase II
	- Chromosomes condense
- Metaphase II
	- Duplicated sister chromosomes come to the middle
- Anaphase II
	- Sister chromatids seperate
- Telophase II
	- Four cells each with one chromatid
	- Haploid

Classical Genetics vs Molecular Genetics
-------------------------

- Classical genetics
	- Focus on inheritance on organismal level
	- See phenotype, try to deduce genotype
- Molecular genetics
	- Concerned with DNA
	- Create mutant genotype, observe phenotypic difference

Medellian Genetics
-------------------------

- Genotype
	- Genetic constitution of an organism
- Phenotype
	- Observable result of the genotype

- Gregor Mendel
	- Performed series of selective breeding experiments with garden peas
		- Peas may self-fertilize
	- After many generations of self-fertilization, "true breeding" or "pure breeding" strains emerged
	- Also crossed gametes between plants

Sept 12
===================

Medellian Genetics Con't
-------------------------

- Mendel selected 7 trainst with pairs of alternative phenotypes
	- Performed series of crosses

- P generation = Parental generation
- F1 gneration = First filial generation
- F2 generation = Results of crossing withing F1
	- Continue *ad infinitum*
- Dominant = trait expressed in a hybrid
- Recessive = trait not expressed in a hybrid

- Monohybrid cross differs with only one trait

- Smooth x Rough --> Smooth
	- Contradicted existing theories of 'blending'
	- F1 not true breeding
	- Deduced that ability to produce rough seeds had not disappeared, but was masked
	- Basically, smooth is dominant over rough

- Mendel concluded that hereditary material (gene) existed in alternate forms (allele)
- True breeders must be homozygous

- To seperate homozygous dominant from heterozygous, perform test cross against homozygous recessive

Basic Probability
-------------------------

- Odds of an event occuring
- Multiply independent events
- Sum the probability of exclusive events

Sept 17
=========================
- Dihybrid cross
	- Organism is heterozygous for two genes
	- 4x4 Punnett Square

- Mendel's Second Law
	- Principal of independant assortment
	- Genes on different chromosomes behave independantly in the production of gametes
	- For *n* independantly assorting genes:
		- 2^*n* phenotypes
		- 3^*n* genotypes

- Chi-Square Test
	- Do observations fit the model, within random chance?

```
Sigma(((Observed - expected)^2)/expected) = Chi-square

Degrees of freedom = n-1 classes

Check against significance table. P is probability that variation is due to chance.

Return p-value corresponding to your Chi-square value.
```
 
Sept 19
=========================

- Binomial Expansion
	- (a+b)^N
		- a,b are probability of alternatives
		- N is number of events
		- (a+b)^2 = a^2 + 2ab + b^2
		- (a+b)^3 = a^3 + 3a^2b + 3ab^2 + b^3

	- Probability = N!/x!(N-x)! * p^x * q^N-x
		- p = probability of outcome
		- q = probability of alternative outcome
		- x = number of times p happened 

Sex Linkage
-------------------------

- Karyotypes of eukaryotes show two types of chromosomes
	- Autosomes are equally represented in both sexes
	- Sex chromosomes are differently represented in different sexes
- Genes located on sex chromosomes are differentially inherited
- "+" inicates wildtype

- Criss-cross inheritance (X-linked)
	- Father --> Daughter -- > Grandson

Sept 24
=========================

Sex Linkage Con't
-------------------------

- Sex linked traits in *Homo sapiens*
	- Colour blindness (X-linked recessive)
	- Faulty tooth enamel (X-linked dominant)
<br></br>
- Most sex-linked traits are X-linked
	- Y is primarily involved in male sex determination
	- A few conditions *are* Y-linked
		- Called holandric (wholly male)
		- By definition, can only be  exhibited by males
<br></br>
- Hairy ears are a possible example of Y-linked
	- Cannot determine dominant/recessive because its irrelevant due to a single copy

Human Inheritance
-------------------------

- Complicated by several factors
	- Controlled matings are unethical
	- Number of progeny are usually small
<br></br>
- Use pedigrees instead
	- Males are squares
	- Females are circles
	- Unaffected are unfilled
	- Affected are filled
<br></br>
- Often difficult to determine pattern of inheritance from a small pedigree

- If a trait is rare, it is possible but unlikely that an unaffected mother is heterozygous

Sept 26
=========================

- Autosomal recessive
	- If recessive trait is rare, wildtype is very likely to be homozygous normal
		- Progeny are heterozygous, trait skips generations
			- Unless trait on both sides of family, or parents are related
		- Matings between two affected individuals give only affected progeny
<br></br>
- Autosomal dominant
	- If trait is deletrious or rare, affected individuals are likely heterozygous
	- Most mating with homozygous normal yield half affected progeny
	- Every affected individual has at least one affected parent
		- No generations skipped
<br></br>
- X-linked recessive
	- Seen mostly in males
	- Affected mother is homozygous
		- Sons guaranteed to be affected
	- Affected male
		- All carrier daughters
		- All normal sons
<br></br>
- X-linked dominant
	- Greater number of females affected
	- Affected mother is most likely heterozygous
		- Half of sons affected, half of females affected
	- Affected male only has affected daughters
	- No generations skipped
<br></br>
- Y-linked
	- Only in males
	- Direct father-son inheritance

- Sex-limited traits
	- Autosomal gene limited to one sex
		- e.g. milk production
- Sex-influenced trait
	- Autosomal trait where relationship between genotype and phenotype differs with sexes
		- Pattern baldness is Human
			- Autosomal trait influenced by hormones
			- Dominant in males
			- Recessive in females

Chromosomal Basis of Sex Determination
-------------------------

- Homologous region on sex chromosomes called "pseudoautosomal region"

- Nondisjunction
	- Failure of homologous chromosomes or sister chromatids to separate during anaphase
	- Can happen with sex chromosomes or autosomes
	- Nondisjunction in Meiosis I
		- 50% have extra chromosome
		- 50% have missing chromosome
	- Nondisjunction in Meiosis II
		- 50% normal
		- 25% have extra chromosome
		- 25% have missing chromosome
	- Called aneuploidy
		- Can be missing or extra
		- Often lethal
		- Can provide evidence of how sex determination works in an organism

Oct 1
=========================

Sex Determination and Dosage Compensation
-------------------------

- Sex Determination
	- Y chromosome mechanism
		- Placental mammals
	- X chromosome - autosome balance system
		- *Drosophila melanogaster* and *Caenorhabditis elegans*
<br></br>
- Mammalian Sex Determination
	- Determined by presence/absence of Y chromosome
	- Evidence provided by individuals with unusual chromosome content
		- X0 = Turner Syndrome
			- 1/10000
			- Female
			- short, infertility, reduced sexual development
		- XXY, XXXY, XXYY = Klinefelter Syndrome
			- 1/1000
			- Male
			- Slightly taller, infertile
		- XYY
			- 1/1000
			- Male
			- Tall, fertile
		- XXX = Triple-X
			- 1/1000
			- Female, slightly tall, normal, fertile
	- Many important genes on X chromosome
		- Cell must equate gene products
		- Mammals inactivate one X chromosome per cell
			- Condensed to a Barr Body
			- Process called Lyonization
			- Also occurs in aneuploids
			- Occurs stochastically at ~1000 cell change
			- Inherited from cell-to-cell 
			- Leads to patchy or mosaic pattern
				- Calico cats
				- Anhidrotic ectodermal dysplasia
				- Werewolf syndrome
	- Presence of Y  will alter some template tissues
		- Gonads + Y = testes
		- Gonads with no Y = ovaries
		- Region producing testes determining factor mapped in sex reversal individuals
			- XX which recombined with Y may carry factor --> phenotypic male
			- XY with factor knockout --> phenotypic female
			- Region contains SRY (sex determining region Y)
				- Necessary and sufficient to determine maleness
<br></br>
- *Drosophila* Sex Determination
	- Ratio based
	- XX/AA >= 1, then female
	- XX/AA = 0.5, then male
	- X0/AA = 0.5, male but sterile
		- Y is required for fertility but not sex determination
	- XX/AAA = 2/3, then intersex but sterile
- *Drosophila* Dosage Compensation
	- Rather than downregulation of X genes, upregulation of Y genes
<br></br>
- *Caenorhabditis elegans* Sex Determination
	- XX = hermaphrodite
		- Normally self-fertilize
	- X0 = Male
		- Sperm have competitive advantage
- *C. elegans* Dosage Determination
	- Each X produces 0.5 

Oct 3
=========================

- Sex Determination in Birds, Butterflies, Snakes
	- Female = ZW
	- Male = ZZ
	- Simmilar idea as mammals, but reversed
	- Bird sex determining gene on Z chromosome
		- Not dosage compensated
		- Males express more than females
			- Threshold controlled, above which testes development proceeds
<br></br>
- Environmental Sex Determination in Turtles
	- \> 32C yields females
	- < 28C gives males
	- between gives mixture

Extensions to Mendellian Inheritance
-------------------------

- Not all traits are completely dominant

- Incomplete Dominance
	- e.g. Snapdragons
		- True breeding P are red x white
			- F1 is pink
- Co-dominance
	- heterozygote exhibits phenotype of both homozygotes
		- e.g. human MN blood group
		- M = L<sup>M</sup> L<sup>M</sup>
		- N = L<sup>N</sup>L<sup>N</sup>
		- MN = L<sup>M</sup> L<sup>N</sup>
<br></br>
- Multiple Alleles
	- human ABO blood type
		- I<sup>A</sub> gives A
		- I<sup>B</sup> gives B
		- i is recessive and gives O
		- O is universal donor
		- AB is universal receiver

- Rh+ dominant over Rh-
	- If mother with Rh-, foetus Rh- at first child
		- Mother produces anti-Rh antibodies
		- 2nd child is attacked, anaemia, death

- Lethal Alleles
	- Mouse coat colour
		- AA is brown
		- A<sup>Y</sup>A is yellow
		- A<sup>Y</sup>A<sup>Y</sup> is lethal
	- If het dies before adulthood, no children
	- If late onset dominant lethal, can still be passed on
		- no symptoms until 30s, die in 40s-50s

End of Midterm Material
-------------------------

Oct 8
=========================

Extensions to Mendellian Genetics
-------------------------

- Two or more genes which affect phenotype
	- combs on chickens
		- rose x single --> 3:1 rose:single
		- pea x single --> 3:1 pea:single
		- rose x pea --> walnut
		- walnut x walnut --> 9:3:3:1 walnut:rose:pea:single
<br></br>
	- squash
		- long breeds true
		- sphere x long --> sphere
		- 9:6:1 disk:sphere:long
		- sphere can be made with either combination of dominant and recessive
<br></br>
- Sometimes a gene can mask the phenotype of another
	- **epistasis**
	- Rodent coat colour
		- agouti : dominant
		- black  : recessive to agouti
		- albino : true breeding, and recessive to any colour
		- agouti x albino -- > agouti x agouti -->
			- 9:3:4 agouti:black:albino
<br></br>
- Penetrance and Expressivity
	- Incomplete penetrance
		- <100% of genetically identical individuals show phenotype
	- Variable expressivity
		- Gradients of phenotype

Oct 10
=========================

- Mapping
	e.g. Tomato
	- P/p Purple/red Dominant/recessive
	- H/h Hairy/smooth Dominant/recessive
	- PPHH x pphh
		- 220 P-H-
		- 210 pphh
		- 32  P-ss
		- 38  ppH-
		- NB parental phenotypes are most common F1 phenotypes

- Simple recombination gives 1/2 parental and 1/2 recombinant progeny

- Likelihood of recombination is proportional to the distance between loci

- RF = recombinants/total progeny = map units = centiMorgans = % chance of recombination during meisosis

- "Coupling"
	- ***cis*** position is when two dominant or two recessive are linked
	- ***trans*** position is when one dominant and one recessive are linked

```
Allele combination can be repressented as:

PH      p+h+      ++
==  OR  ----  OR  --
ph      p h       ph

PH/ph  OR  p+h+/ph  OR  ++/ph
```
 
Oct 22
=========================

- Linked genes are on the same chromosome
	- < 50 cM
- Distinguish by examining dihybrid test cross progeny

- Unliked genes give 50% recombination

- Genes on same chromosome but more than 50 cM apart behave like unlinked genes

- Can get distances above 50 cM by addition, but cannot determine by recombination frequencies

- Double crossovers may regenerate parental genotype
	- Every crossover only involves 2 strands

- Even mutiple crossover events generate on average
	- 50% recombinants
	- 50% parental
	- **Cannot** measure beyond 50 cM for two genes

- Three genes to get beyond this
	- Can determine distance and order
 

white eyes w
singled bristles sn
miniature wings m

 w sn m<sup>+</sup>		w<sup>+</sup>sn<sup>+</sup>m
------------------  X  ------------------------------
 w sn m<sup>+</sup>					Y

 F1 w sn m<sup>+</sup>		w sn m<sup>+</sup>
   ------------------   X   -------------------
 w<sup>+</sup>sn<sup>+</sup>m 	Y

Essentially no recombination in Drosophilia males (X or autosome)

	1. Identify parental classes
	2. Identify recombinant classes (similar frequencies and complemtary phenotypes)
	3. The gene that switches position relative to the other two is the middle gene
```
Coefficient of Coincidence = (double crossover events observed) / (double crossover events expected)

Interference = 1 - CoC
```
- Positive interference when I is 0 < I < 1
- Negative is when I is > 1

Oct 24
=========================

Changes in Chromosome Structure and Number
-------------------------

- Basic rules
	- Homologous chromosomes will attempt pairing during Prophase I
	- Spindles attach to centromeres
	- Homologous chromosomes seperate during Meiosis I
	- Chromosomes seperate in Meiosis II
<br></br>
- Structural changes
	- Initiated by double-stranded chromosome breaks
		- Caused by radiation, chemicals, recombination errors
	- Ends are sticky
	- Results in change in conformation
		- **Deletion**
			- Loss of centromere means loss of whole chromosome
				- Typically fatal
			- Deletion of individual regions *may* be okay if not homozygous for deletion
			- If homologue is recessive type, triggers "pseudodominance"
				- Unexpected expression of recessive allele
			- Can be triggered by recombination with chromosome without homologous region
		- **Duplication**
			- Doubling of a chromosome segment
			- Can arise from unequal crossover
				- An existing duplications can make subsequent duplications more likely
			- Better tolerated than deletions
			- "Correct" homologue will pair with either copy of duplicated regions
			- Important for evolution and expansion of gene families
		- **Inversion**
			- Chromosomal segment is excised and reintegrated in the opposite direction
			- Pericentric inversions contain centromere
			- Paracentric inversions do not contain the centromere
			- Heterozygotes must create **inversion loop**
				- Forms dicentric bridge and acentric fragment
				- Acentric fragment is lost
				- 50% of gametes are viable (parental genotype)
		- **Translocation**
			- Movement of segment to another point in the genome
			- No net gain or loss of genetic material
			- 50% gametes are inviable
<br></br>
- Number changes
	- Human aneuploidy in autosomes is generally lethal
		- Trisomy 21 (Down's Syndrome) is an exception
	- Aneuploidy in sex chromosomes is tolerated due to dosage compensation via Barr Bodies
	- All aneuploidy in plants in a 2N+1 are equally likely

Oct 29
=========================

- In eusocial insects (bees, wasps, ants), males are monoploid
	- Produce gametes by mitosis
	- Develop from unfertilized eggs

- Many plants are polyploid
	- Bread wheat are hexaploid
	- Strawberries are octaploid

- Autopolyploidy is all chromosome from the same species
- Allopolyploidy has chromosomes from different, but related species
	- Almost always sterile
	- If chromosomes double, but fail to segregate, offspring are fertile

- Bananas
	- Random segregation of every chromosome
	- Every gamete has different chromosome count
	- Gametes have different chromosome counts and are inviable
	- All modern commerical bananas are genetically identical

- **Population** is a group of interbreeding individuals of the same species

- **Gene pool** is the total of all alleles possessed by members of a population

- For sex-linked genes, it is best to use gene counting methods
	- (Number of chromosomes with trait) / (total number of chromosomes)

- Hardy-Weinberg Law (Ideal Population)
	- no selection
	- no mutation
	- no migration
	- population is infinitely large
	- individuals mate at random

p<sup>2</sup> + 2 pq + q<sup>2</sup> = 1

	- Population where this is true is at Hardy-Weinberg Equilibrium
	- Can introduce r term for multiple alleles

p<sup>2</sup> + q<sup>2</sup> + r<sup>2</sup> + 2pq + 2pr + 2qr = 1

- In general, when testing Hardy-Weinberg, the degrees of freedom == (phenotypes - alleles)

Oct 31
=========================

Non-ideal Populations
-------------------------

- Genetic Drift
	- Caused by small populations
	- Random 
	- Causes **fixation**
		- Populations arise that are all homozygous
		- Loss of alleles
<br></br>
- Nonrandom Mating
	- Inbreeding & Self-fertilization
		- Increase in homozygotes
		- No loss of alleles
<br></br>
- Mutation
	- Creates new alleles
	- Provides raw material for selection
	- Very slow on its own
<br></br>
- Migration
	- homogenizes allele frequencies across populations
	- increased heterozygosity
	- P<sub>y</sub>` = m * P<sub>x</sub> + (1-m) * P<sub>y</sub>
<br></br>
- Selection
	- Fitness (w) = relative number of progeny in the next population
	- Largest genotype is 1; lethal is 0

Quantitative Genetics
-------------------------

- Many traits are continuous
- If trait is under the control of multiple genes with two alleles
	- Called polygenic 
	- 3<sup>n</sup> possibilities; n = genes

- Environment may also influence traits
	- Called multifactorial trait
	
Nov 5
=========================

Continuous Traits
-------------------------

- Take specific measurements
	- Entire group of interest
		- Impractical, so take representative subset

- Plot as a frequency distribution
	- Histogram
	- Generate curve
	- Look for normal distribution
		- Typical of multifactorial traits

- Use statistics to describe 
	- Variance gives units<sup>2</sup>
	- Standard deviation gives answer in original units
	- Mean gives centre of curve

- Majority of agricultural traits are quantitative

- Contributing allele
	- Contributes to a quantitative traits
- Non-contributing allele
	- Does not affect quantitative trait

- Probability
	- Use complete binomial expansion
(a+b)<sup>3</sup>
	- Step 1: a<sup>3</sup>+a<sup>2</sup>b+ab<sup>2</sup>+b<sup>3</sup>
	- Step 2: Get Coefficients from Pascal's Triangle (n+1)
		- a<sup>3</sup>+3a<sup>2</sup>b+3ab<sup>2</sup>+b<sup>3</sup>
	- Assumes no epistasis, linkage, environmental effects, genes are fully contributing or non-contributing

Nov 7
=========================

Continuous Traits Con't
-------------------------

- Covariance
	- Sigma(x<sub>i</sub> - x<sub>mean</sub>)(y<sub>i</sub> - y<sub>mean</sub>) / n-1
	- Determines if variables tend to vary consistently from the mean
<br></br>
- Correlation coefficient (r)
	- r = COV<sub>xy</sub> / (s<sub>x</sub> * s<sub>y</sub>)
	- 1.00 = high positive correlation
	- 0.00 = no correlation
	- (-1.00) = high negative correlation
<br></br>
- Heritability
	 - Measure of genetic contribution to phenotypic variance (V<sub>p</sub>)
	 - V<sub>p</sub> = V<sub>G</sub> + V<sub>E</sub>
	 - V<sub>G</sub> is genetic variance
	 - V<sub>E</sub> is environmental variance
<br></br>
- Broad-sense heritability
	- H<sub>B</sub><sup>2</sup> = V<sub>G</sub> / V<sub>p</sub> = V<sub>G</sub> / V<sub>G</sub> + V<sub>E</sub>
	- If H<sub>B</sub><sup>2</sup> is close to 1.00, environmental factors have little impact
	- If H<sub>B</sub><sup>2</sup> is close to 0.00, environmental factors is primary source of variation
<br></br>
- Narrow-sense heritability
	- H<sub>N</sub><sup>2</sup> = V<sub>A</sub> / V<sub>p</sub>
	- Also called "realized heritability"
	- Used in breeding programs to assess results of selection procedures
	- H<sub>N</sub><sup>2</sup> = (selection response) / (selection differential) 
<br></br>
- Quantitative Trait Mapping (QTL)
	- Take two strains which differ in a given trait
	- Cross parents to make F1; F1 should produce intermediate phenotype
	- Generate F2; similar to F1, but broader range of data
	- Examine correlation between phenotype and genotype in most extreme progeny

Structure of DNA
-------------------------

- Nucleotides
	- Sugar
		- 5 carbon sugar (ribose | deoxyribose)
		- 1' position binds base
		- 2' is OH or H
		- 3' attaches next nucleotide in chain
		- 5' binds phosphate
	- Base
		- purines have two rings
			- Adenine and Guanine
		- pyrimidine have one ring
			- Cytosine, Uracil, and Thymine
	- Phosphate
		- Phosphodiester bond joins nucleotides
		- 3' - 5' 

Nov 12
=========================

- DNA double helix
	- Anti-parallel
	- Sugar-phosphate backbone faces outward
	- Bases held together by hydrogen bonds
	- GC pairs are more stable that AT
		- 3 H bonds instead of 2
		- %G == %C
	- 10 bp / turn
		- 0.34 nm / bp
	- Semi-conservative replication
<br></br>
- DNA polymerases
	- Needs:	
		- DNA template
		- Free dNTPs
		- primer
	- Always runs 5'->3'
	- Energy for synthesis comes from hydrolysis of 2 PO<sub>4</sub>
		- Releases PP<sub>i</sub>

```
Pol		Synthesis 	3-5 exo 	5-3 exo Processivity 	Function
I 		Yes 		Yes 		Yes 	Low				Replication & Repair
II 		Yes 		Yes 		No 						Repair
III 	Yes 		Yes 		No 		High 			Replication
```
 
DNA Replication
-------------------------

- **Helicase**
	- Unwinds DNA
	- Creates ss regions
- **Single strand binding proteins (SSB)**
	- Bind and stabilize ssDNA
- **Primase**
	- Synthesizes short RNA primer so 3'OH available for extension
- **DNA Pol III**
	- Synthesizes DNA from primer 3'OH
	- Falls off when it encounters an RNA primer
- **DNA Pol I**
	- 5'-3' exonuclease activity removes primers and replaces with DNA
- **DNA ligase**
	- Patches gaps between Okasaki fragments

Nov 14
=========================

Transcription in Prokaryotes
-------------------------

- **Initiation**
	- Consensus sequences at -10 (TATAAT) and -35 (TTGACA) bind RNA pol and sigma factor
	- Transcription initiates at +1
	- 5'-3'
<br></br>
- **Elongation**
	- Sigma factor is released
	- 40 nucleotides per sec
<br></br>
- **Termination**
	- Two types of termination
		- Intrinsic (Rho-independent)
			- GC Hairpin in transcript followed by poly-U dissociates RNA pol
		- Rho-dependent
			- Requires rho helicase
			- Rho binds C-riich terminator sequence
				- Unwinds DNA-RNA hybrid
				- RNA pol pauses and dissociates  
<br></br>
- *E. coli* RNA polymerase
	- Makes all types of RNA
	- Complex of core enzyme, sigma factor (required for initiation)

Transcription in Eukaryotes
-------------------------

- Transcription takes place in nucleus
	- Translation in cytoplasm
<br></br>
- RNA polymerases
	- RNA Pol I makes rRNA
	- RNA Pol II makes mRNA
	- RNA Pol III makes tRNA
<br></br>
- Promoter is -30 (TATA box)
	- bound by general transcription factors, then RNA Pol II
	- Sufficient for low level transcription
	- **Enhancer** sequences may be bound by activator proteins to upregulate
	- **Silencer** sequences may bind repressor protein for downregulation
<br></br>
- RNA processing
	- Addition of 5' cap
		- Protects mRNA from degradation
		- Finds ribosome to initiate translation
		- Occurs when transcript is 20-30 nt long
		- 7-methyl-1-guanosine (m<sup>7</sup>G)
<br></br>
	- Addition of poly-A 3' tail
		- Regulates message stability and export
		- Sequence AAUAAA signals RNase to cut 10-30 nt later-
		- Poly-A polymerase adds 50-250 As to 3' end
	- Splicing
		- Introns are removed
			- Usually flanked by GU and AG
		- Mechanism 1
			- 5' cleavage leads to formation of "lariat"
			- Branch point has unusual 2'-5' linkage
			- 3' intron cleavage
			- Exon ligation

Translation
-------------------------
```
Amino acids:

	   R
	   |
H3N -- C -- COO-
```

- Four groups
	- Acidic (Asp, Glu)
	- Basic (Lys, Arg)
	- Polar (Ser)(-OH)
	- Non-polar (Ala)(-CH<sub>3</sub>)

- Protein Structure
	- Primary Structure
		- Linear amino acid sequence
	- Secondary Structure
		- 3D structural motif
			- Alpha helices
			- Beta Sheets
	- Tertiary Structure
		- Overall shape due to folding
	- Quaternary Structure
		- Protein complexes

Nov 21
=========================

Restriction Enzymes
-------------------------

- Endonucleases that cut dsDNA at specific sequences

- Primarily bacterial in origin
	- Protect genome from bacteriophages
	- Guided by methylation

- Name is derived from source organism
	- e.g. EcoRI is from *E. coli*, strain **R**413, first to be found there

- Site often has 2-fold symmetry
	- same on both strands
 
```
5'-GAATTC-3'
3'-CTTAAG-5' 
```
 
- Probability of a specific base raised to the power of site length
	- (1/4)<sup>length</sup>
	- Calculation assumes equal abundance of all four bases
		- i.e. GC = 50%
	- Adjust probabilities for GC content
		- e.g. If GC = 60%, GAATTC is (0.3)<sup>2</sup>(0.2)<sup>4</sup> 

- Cut ends can be blunt or sticky

- Restriction mapping
	- Digest DNA with restriction enzymes
	- Visualize by gel electrophoresis
		- DNA is negative, moves toward the positive electrode
		- Agarose is like seive
			- Small fragments move fastest
		- Intercalating dye (EtBr or SYBR Green)

- Cloning Recombinant DNA
	- Cut gene of interest with restriction enzyme
	- Cut recipient DNA with restrition enzyme
		- Usually a cloning vector
	- Use DNA ligase to join sticky ends
		- Works with blunt ends as well, but is less efficient
	- Transform into competent host cells
		- Use selection protocol to select for recombinants
<br></br>
- Cloning vectors
	- Plasmids
		- Smallest and most used
		- Circular dsDNA
		- Replicate independently of chromosome
		- Have defined *ori* sequence
		- Use dominant selectable marker like AMR gene
		- Polylinker is located in middle of gene like LacZ
			- Insertion interupts LacZ
				- LacZ in presence of X-gal produce blue colonies
				- LacZ<sup>-</sup> will produce white colonies
			- Cells which are LacZ<sup>-</sup> and resistant were successful transformants
	- BAC
	- YAC
	- Cosmid