Sept 9
=========================

Languages
-------------------------

- Declaration statements
	- Not in Python
	- Included in Java to improve error safety

- Assignments and conditional execution are required for *all* programming languages

- Assignment statements
	- Variable on the left, value on the right
	- in Java, variables must always begin with lower case

- Decision statements
	- Control
	- Execute some statements if a condition is true
	- Loops are repeating execution statements

- Use of functions/**methods**/procedures/subroutines
	- Break program into discreet subcomponents
	- Reusable
	- A value is returned, or can be void

- Compilation vs interpretation
	- Java is compiled to bytecode, then run by the JVM
	- Python is interpreted directly from source

Java Program Structure
-------------------------
- Best to use GNU-style braces (braces on own line)
- Class name must match file name
- Semicolons end statements
 
```
public class HelloWorld
{
	public static void main(String[] args)
	{
		System.out.println("Hello, World!");
	}
}
```
Sept 16
=========================

Growing Programs
-------------------------

- Always Be Running
	- Re-compile and Run for each block

Sept 18
=========================

Find max value in an array:

```
int result = list[0];
for (int count = 0; count < list.length;count++ )
{
	if (result < list[count])
	{
		result = list[count];
	}
}
return result;
```

Sorting a list:
```
public static void stortlist1(int[] list)
{
	int temp, count;
	for (count = 1; count < list.length, count++) // has to be 'count = 1' else program will look for list[-1]
	{
		if(list[count-1]>list[count])
		{
		temp = list[count-1];
		list[count-1] = list[count];
		list[count] = temp;
		}
	}
}
```
 
Objects
-------------------------

- In Java, each variable must be called with a data type
- Primitives:
	- int, double, short, long, char, float, byte, boolean
- Other data types are Objects
	- In Java, have uppercase names
		- e.g. String
		- dot notation gives properties of object instances
- Object either definted by the programmer or are included with the system

Sept 20
=========================

- Default login for nTreePoint
	- umid / student number
- To remove numbers for Sieve of Eratosthenes array, increment by {2,3,5,7} and set values to zero
	- Much faster than divison or modulo

Sept 23
=========================

- Constructor is a method without a return type located within the class
 
```
fred = new Student(100,"Fred",3.0) // usage elsewhere

class Student
{
	private int number; // private means variable can only be referred to within the class and not external calling classes
	private String name; // prevents accidental modification of values
	private double gpa;	// in general, variables within classes should be private

	public Student(int number, String name, double gpa) // this is the constructor
	{
		this.number = number;
		this.name = name;
		this.gpa = gpa;
	}
	public String toString()	// print and println will look for toString() to print and should always be included in a class 
	{
		return number + " " +name+" "+gpa; // note that there's no actual printing, just a return
	}

	public double getGPA()	// will provide the value of a private variable without allowing modification; called an Accessor
	{
		return gpa;
	}
}

```

Sept 27
==========================

- Instance (non-static) variables are for when multiple objects are being created from a class
- Static variables are for something only for internal use within a method

```
try
{
	BufferedReader filein;
	String line;

	filein = new BufferedReader (new TextReader("in.txt"));
	line = filein.readline();
}
catch (IOException ioe)
{
	System.out.println(ioe.getMessage());
	ioe.printStackTrace();
}
```

Sept 30
=========================

- Hand in paper assignment in E2-418 in a folder
- Submit electronic version seperately on course website
- Honesty declaration goes in during class

```
import java.io.*;

PrintWriter outputFile;

outputFile = new PrintWriter(new FileWriter("out.txt"));

outputFile.println(any values);

outputFile.close(); // required
```
 
Oct 2
=========================

- Moving file processing to a class is a good idea
 
```
class Reader
{
	
	private String filename;
	BufferedReader file;
	file = new BufferedReader(new FileReader(filename));

	public Reader (String filename)
	{

		this.filename = filename; 
	}
	public String readLine()
	{
		line = file.readLine();

		return line;
	}
}

```
 
Oct 4
=========================

- String contents are stored as ```char``` array

- ```Split()```
	- Takes contents of a String and returns the tokens that are inside a String

```
String x = "123  	Fred";

String[] tokens;
tokens = x.split("\\s+"); //Split on whitespace -- same as tokens = x.split("\s+")) in Python

tokens = x.trim().split("\\s+"); //Trims outer whitespace, then splits
```
 

Oct 7
=========================

- ```.length``` is used for arrays

- ```.length()``` is used for Strings

- ```.size()``` is used for collections

- ```.equals()``` to compare Strings
	- ```string1.equals(string2)``` --> ```boolean```

- Search and Replace
	- ```.indexOf(string)```
		- returns ```int```
	- ```.replace(string1,string2)```
		- ```"fred".replace("ed","ank")``` --> ```"frank"```
		- replaces all occurances, not just first
	-```.replaceFirst(string1,string2)```
		- replaces first instance only

Oct 9
=========================

- Collections
	- A collection is a data structure

- Arrays cannot be resized
	- Copy relevant elements into a new array
		- ```System.arraycopy()```

- ArrayList
	- Pre-defined class in System
	- Works like ```list``` in Python

``` 
	import java.util.ArrayList;

	ArrayList list;
	list = new ArrayList(); // list is empty -- list.size() returns 0
	list.add("abc"); // same Python's list.append()
	String string1 = list.get(0); // returns "abc"
	list.set(0, "xyz");
	list.get(0) // returns "xyz"
```
 
Oct 11
=========================

- Arrays are objects implemented by the Java compiler
	- Not defined in a class
	- list[3] // array notation

- ArrayList is a true object
	- Defined in a class
	- Dynamic in size
	- list.get(3) // ArrayList notation; is a method
		- Must cast return to desired datatype
	- list.add(3, "String10"); // inserts second parameter at index first parameter

```@SuppressedWarnings("unchecked")``` suppresses warnings about mixed datatypes in ArrayList

- Generics
	- ArrayList<Object> list;
		- ArrayList <String> will only store Strings
		- This only works with objects
			- Use wrapper classes for primitives
				e.g. ```Integer``` for ```int```
		- Program is now safer

- Automatic conversion between wrappers and primitives is called "Boxing" and "unboxing"
 
 Oct 16
 ========================

 - Boxing is the automatic conversion of a primitive to the the corresponding wrapper object
 - Unboxing is automatic conversion of wrapper object to its primitive type

 Oct 18
 ========================

 - Exam topics
 	- Data types
 		- Primitives
 		- Arrays
 		- Provided/implemented by the compiler
 	- Compiler
 		- Source --> compiler --> Byte code --> JVM --> Machine code
 	- Control flow
 	- Static methods for procedural programming
 	- Instance methods for objects
 		- Prefixed with the object name
 	- Class defines a data type
 		- Any defined but uninstantiated object returns null value
 	- .equals is for object comparison
 	- ```==``` used for primitive comparison

 Oct 21
=========================

- Inheritance
 
```
class Animal
{
	public String name;
	public String type;

	public String toString()
	{
		return "Animal: Type: "+type+"; Name: "+name;
	}
}
class Cat extends Animal
{
	public Cat (String name, String type)
	{
		this.name = name;
		type = "cat";
	}
	public void makeNoise()
	{
		System.out.prinln("meow");
	}
}
class Dog extends Animal
{
	public Dog (String name, String type)
	{
		this.name = name;
		type = "dog";
	}
	public void makeNoise()
	{
		System.out.prinln("woof");
	}
}
```
 
- Animal above is a superclass
	- Cat and Dog are subclasses

Oct 23
======================

- If a method is repeated in the superclass and the subclass, the subclass version will be used
	- Called overriding

- Protected variables can be accessed by subclasses
	- Private variables cannot

- call ```super()``` in subclass constructor to pass to superclass constructor

```
class Dog extends Animal
{
	public Dog (String name)
	{
	super(name,"dog");
	}
}
```
 
- A collection of the superclass can store the subclasses
```
Animal[] zoo = new Animal[2];
zoo[0] = new Dog("Fido");
zoo[1] = new Cat("Fluffy");
```
 
- Protected constructor prevents objects of the superclass from being instantiated

Oct 28
=========================

- Recursion
	- Supports a different type of loop
	- Defined in terms of itself

- For and while loops are iterative loops

```
public static int sum (int n)
{
	int result;

	if (n == 1)
	{
		result = 1;
	}
	else
	{
		result = n + sum(n-1)
	}
	return result;
}

Oct 30
=========================

- The Stack
	- Push means add to the stack
	- Pop means remove from the stack
	- Temporarily accessible memory

- Every method called is pushed to the stack

- The Heap
	- Stores objects
	- Long term

- Base case is condition which requires no further recursion
	- Returns value

- Iterative methods works much faster

Nov 1
=========================

```
public static printList(int[] list, int position)
{
	if (position < list.length)
	{
		System.out.println(list[position]);
		printList(list, position+1)
	}
	else
	{
		System.out.println();
	}
}
```
 
- Use interface methods to pass parameters that the user does not care about to a method
```
public static printList(int[] list)
{
	printList(list, 0)
}
```
- Don't use ++ in recursion calls

Nov 4
=========================

- Bubble sort is slowest of all sorts
- Double Bubble Sort is slightly faster
	- Moves larger going right
	- Smaller going left

- Selection sort
	- Keeps track of which element is largest

Nov 6
=========================

- Bubble, Selection sort, and Insertion sorts on final
	- Insertion sort is fastest

Nov 8
=========================

- Bubble sort is O(n<sup>2</sup>)
- Insertion sort is O(n) if already sorted
- Quicksort 
	- one of the fastest

Nov 12
=========================

A4Q3
-------------------------

- Not a duplicate if current index == last-1
- NO for loops; Method must be recursive
- ArrayList has a method that searches from right to left

Object Representation
-------------------------

- Every variable has a location in memory allocated to it
- int gets one **word** (32 bits)
- long gets two words (64 bits)
- All primitives have their value stored in the memory location allocated for the associated variable
- Objects have pointers to the object 
	- Pointers are stored in the associated variable, and point to the actual object
	- Pointers are one word long (32 bit)
	- Object with no pointer is called **orphan**; cannot be dropped by Java for safety reasons
		- Garbage collection is the process of identifying orphans are returning their memory as usable space
	- ```student2 = student1``` copies the pointer, not the object
			- Cloning copies the contents of an object to a new object

Nov 18
=========================

Linked lists
-------------------------

- Create a LinkedList class
	- Should support all the same API functions as ArrayList
	- Contains the nodes
- Use a node class
	- Each node class stores two pointers
		- Pointer to associated object
		- Pointer to next node
- Deals only with pointers, not physical memory adjacency

- Encapsulation
	- Keeping the internal details hidden from the programmer 

- LinkedList.get(position)
	- loop until count == position
	- Return pointer from node to associated object
- LinkedList.remove(position)
	- Find the node at the specified position
	- Store pointer to position+1 in position-1
	- Must handle previousNode == null

- Testing
	- Ensure all variables have appropriate values
		- Check that all parameters passed to each method contain valid values
	- LinkedList.get(position)
		- Ensure position is valid
			- >= 0
			- < size()
			- If parameter is invalid:
				- Print an error message
				- Return null or -1
			- Don't check for invalid values in calling program
		- Normally you check for null pointer, but will not be possible if LinkedList is initialized correctly

n-Dimensional Arrays
-------------------------

- Mainly for scientific and mathematical processing
 
```
int[][] array;

array = new int[3][2];

array[0][0] = 23;
array[0][1] = 42;

23	42
0	0
0	0

```
 
Nov 22
=========================

- Passing Parameters
	- Primitives
		- Java makes copies of the values of parameters
		- Copy is passed to called method
		- Returning throws away copied variables
	- Objects
		- Changes are reflected in the original
			- Magic of pointers
				- Pointer is thrown away, as with primitives, but the object was modified
			- e.g. Can modify array in a method

Nov 25
=========================

- Cannot reinstantiate objects passed into a method
	- Copied variable points to the new array and is lost after the method closes

- String are immutable
	- Once a value is assigned, it cannot be changed
	- If String is modified, will point to a new object

- Wrapper classes are immutable

- StringBuffer is more efficient for large programs
	- Can mutable objects

Nov 27
=========================

- Assignment 5
	- Linked List
		- Get- same as an ArrayList
		- Size - no loops
			- Keep size variable inside class

Dec 2
=========================

- Exam
	- 2 hours
	- Dec 10 @ 1:30, FK Brown
	- 8 - 10 questions
	- No algorithm analysis questions
	- Mostly classes and methods
		- Object orientation
			- I/O
				- Include try/catch
					- Simple error output
			- String and ArrayList classes
			- Instance variables, constructors, toString, etc
		
		- Procedural programming
			- Decision statements
			- Loops
			- Recursion
			- Method calls

		- Array processing
			- System.arraycopy()
			- Arrays.toString()
			
		- Object representation
			- Just need to know its impact

		- Collections
			- Array
			- ArrayList
			- LinkedList